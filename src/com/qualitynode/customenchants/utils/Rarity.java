package com.qualitynode.customenchants.utils;

import com.qualitynode.customenchants.CustomEnchants;

import java.util.HashMap;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 */
public enum Rarity {

    Common("&f", 500, "19 levels"),
    Uncommon("&a", 2000, "34 levels"),
    Epic("&b", 5000, "48 levels'"),
    Legendary("&e", 15000, "74 levels"),
    Mystical("&6", 30000, "98 levels");

    private static Map<Rarity, String> colorCodes = new HashMap<>();
    private static Map<Rarity, String> displays = new HashMap<>();
    private static Map<Rarity, Integer> expCosts = new HashMap<>();

    private String _prefix, display;
    private int _cost;

    Rarity(String prefix, int cost, String display) {
        _prefix = prefix;
        _cost = cost;
        this.display = display;
    }

    public String getDefault() {
        return _prefix;
    }

    public String getPrefix() {
        return colorCodes.get(this);
    }

    public String getDefaultDisplay() {
        return this.display;
    }

    public String getDisplay() {
        return displays.get(this);
    }

    public int getDefaultCost() {
        return _cost;
    }

    public int getCost() {
        return expCosts.get(this);
    }

    public static void setValues(CustomEnchants plugin) {
        colorCodes.clear();
        expCosts.clear();

        for(Rarity rarity : Rarity.values()) {
            String name = rarity.name();

            colorCodes.put(rarity, plugin.getConfig().getString("CustomEnchants.Rarity." + name + ".color", rarity.getDefault()));
            expCosts.put(rarity, plugin.getConfig().getInt("CustomEnchants.Rarity." + name + ".cost", rarity.getDefaultCost()));
            displays.put(rarity, plugin.getConfig().getString("CustomEnchants.Rarity." + name + ".display", rarity.getDefaultDisplay()));
        }
    }

    public static Rarity getRarity(String name) {
        for(Rarity rarity : values()) {
            if(rarity.name().equalsIgnoreCase(name)) return rarity;
        }

        return null;
    }
}
