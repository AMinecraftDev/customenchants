package com.qualitynode.customenchants.utils;

import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 */
public enum EnchantmentType {

    Helmet("_HELMET"),
    Chestplate("_CHESTPLATE"),
    Leggings("_LEGGINGS"),
    Boots("_BOOTS"),
    Armour(Helmet.getEndsWith(), Chestplate.getEndsWith(), Leggings.getEndsWith(), Boots.getEndsWith()),

    Sword("_SWORD"),
    Axe("_AXE"),
    Bow("BOW"),
    Weapon(Sword.getEndsWith(), Axe.getEndsWith(), Bow.getEndsWith()),

    Pickaxe("_PICKAXE"),
    Shovel("_SPADE"),
    Tool(Pickaxe.getEndsWith(), Shovel.getEndsWith()),

    All(Armour.getEndsWith(), Weapon.getEndsWith(), Tool.getEndsWith());

    private List<String> endsWith = new ArrayList<>();

    EnchantmentType(String endsWith) {
        this.endsWith.add(endsWith);
    }

    EnchantmentType(List<String>... endsWithList) {
        for(List<String> s : endsWithList) {
            this.endsWith.addAll(s);
        }
    }

    private List<String> getEndsWith() {
        return endsWith;
    }

    public boolean doesMaterialEndWith(Material material) {
        for(String s : this.endsWith) {
            if(material.toString().endsWith(s)) return true;
        }

        return false;
    }

}
