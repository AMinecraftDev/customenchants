package com.qualitynode.customenchants.utils.core;

import net.aminecraftdev.utils.message.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Oct-17
 */
public enum Message {

    DATE_FORMAT("GeneralErrors.date-format", "MM-d-yyyy hh:mm:ss"), // 3/31/15 07:49AM
    NO_PERMISSION("GeneralErrors.NoPermission", "&c&l(!) &cYou do not have permission to use that."),
    MUST_BE_PLAYER("GeneralErrors.MustBePlayer", "&c&l(!) &cYou must be a player to use that."),
    INVALID_INTEGER("GeneralErrors.InvalidNumber", "&c&l(!) &cYou specified an invalid number, try something like 1 or 5000."),
    INVALID_DOUBLE("GeneralErrors.InvalidDouble", "&c&l(!) &cYou specified an invalid double, try something like 1.4 or 2.3."),
    INVENTORY_SPACE("GeneralErrors.InventorySpace", "&c&l(!) &cYou do not have enough inventory space."),
    NOT_ONLINE("GeneralErrors.NotOnline", "&c&l(!) &cThe specified player is offline."),
    ITEMSTACK_MIN("GeneralErrors.MinItemStack", "&c&l(!) &cThe minimum ItemStack size is 1."),
    ITEMSTACK_MAX("GeneralErrors.MaxItemStack", "&c&l(!) &cThe maximum ItemStack size is 64."),
    ITEMSTACK_NULL("GeneralErrors.NullItemStack", "&c&l(!) &cThe specified ItemStack cannot be null or air."),
    INSUFFICIENT_FUNDS("GeneralErrors.InsufficientFunds", "&c&l(!) &cYou have insufficient funds for that transaction! You need at least another ${0}&c."),
    MONEY_TAKEN("GeneralErrors.MoneyTaken", "&c&l- ${0}"),
    MONEY_GIVEN("GeneralErrors.MoneyGiven", "&a&l+ ${0}"),

        CustomEnchants_AlreadyHaveEnchant("&c&l(!) &cYou already have that enchant on that item."),
                CustomEnchants_Apply_NoDestroy("&c&l(!) &cThe application of the enchant failed however your item wasn't destroyed."),
                CustomEnchants_Apply_NoWhiteScroll("&c&l(!) &cThe application of the enchant has failed and you did not have a whitescroll on it to protect it."),
                CustomEnchants_Apply_Successful("&3You have successfully applied the book the itemstack."),
                CustomEnchants_Apply_WhiteScroll("&c&l(!) &cThe application of the enchant has failed, however the whitescroll has protected your item."),
                CustomEnchants_BlackScroll_Apply("&3You have applied the blackscroll to them item and an enchantment was removed."),
                CustomEnchants_BlackScroll_Purchased("&3You have purchased a blackscroll."),
                CustomEnchants_Book_Revealed("&3You have uncovered your mystery enchantment book!"),
                CustomEnchants_CannotEnchant("&c&l(!) &cYou cannot enchant that item with that enchant."),
                CustomEnchants_CustomItems_BlackScroll_Given("&3You have given &b{0} {1}x BlackScroll(s)&3."),
                CustomEnchants_CustomItems_BlackScroll_InvalidArgs("&c&l(!) &cInvalid args! &nTry /cusi blackscroll [player] [amount]&c."),
                CustomEnchants_CustomItems_BlackScroll_Received("&3You have received &b{0}x BlackScroll(s)&3."),
                CustomEnchants_CustomItems_Book_EnchantNotExist("&c&l(!) &cThat enchantment does not exist or is not enabled."),
                CustomEnchants_CustomItems_Book_Given("&3You have given &b{0} {1}x {2} CustomEnchant Book(s)&3."),
                CustomEnchants_CustomItems_Book_InvalidArgs("&c&l(!) &cInvalid args! &nTry /cusi book [player] [enchant] [level] [amount] [success] [destroy]&c."),
                CustomEnchants_CustomItems_Book_Received("&3You have received &b{0}x {1} CustomEnchant Book(s)&3."),
                CustomEnchants_CustomItems_Dust_Given("&3You have given &b{0} {1}x {2}% {3} Dust&3."),
                CustomEnchants_CustomItems_Dust_InvalidArgs("&c&l(!) &cInvalid args! &nTry /cusi dust [player] [rarity] [percentage] [amount]&c."),
                CustomEnchants_CustomItems_Dust_RarityNotExist("&c&l(!) &cThat rarity does not exist or is not enabled."),
                CustomEnchants_CustomItems_Dust_Received("&3You have received &b{0}x {1}% {2} Dust&3."),
                CustomEnchants_CustomItems_EnchantNotExist("&c&l(!) &cThe specified enchant does not exist!"),
                CustomEnchants_CustomItems_InvalidArgs(
                        "&3&m-------&b&l CUSTOMENCHANTS HELP &3&m-------\n" +
                                "&c/cusi item [itemType] (player:<name>) (name:<item name>) (amount:<item amount>) (lore:<lore with _ for spaces and /n to specify next line>) (<enchantName>:<level (put ? for random)>)\n" +
                                "&c/cusi book [player] [enchant] [level] [amount] [success] [destroy]\n" +
                                "&c/cusi ubook [rarity] [player] [amount]\n" +
                                "&c/cusi dust [player] [rarity] [percentage] [amount]\n" +
                                "&c/cusi blackscroll [player] [amount]\n" +
                                "&c/cusi whitescroll [player] [amount]\n" +
                                "&c/cusi transmog [player] [amount]\n" +
                                "&c/cusi [enchant] [level]"),
                CustomEnchants_CustomItems_Item_MaterialNull("&c&l(!) &cThe specified material is null, an example is 'AIR' or 'STONE'."),
                CustomEnchants_CustomItems_Item_TargetOffline("&c&l(!) &cThe targeted player is offline!"),
                CustomEnchants_CustomItems_UBook_InvalidRarity("&c&l(!) &cYou have specified an invalid rarity!"),
                CustomEnchants_CustomItems_UBook_Given("&3You have given &b{0} {1}x {2} Unopened Book(s)."),
                CustomEnchants_CustomItems_UBook_Received("&3You have received &b{0}x {1} Unopened Book(s)."),
                CustomEnchants_CustomItems_WhiteScroll_Given("&3You have given &b{0} {1}x WhiteScroll(s)&3."),
                CustomEnchants_CustomItems_WhiteScroll_InvalidArgs("&c&l(!) &cInvalid args! &nTry /cusi whitescroll [player] [amount]&c."),
                CustomEnchants_CustomItems_WhiteScroll_Received("&3You have received &b{0}x WhiteScroll(s)&3."),
                CustomEnchants_Dust_AlreadyMaxed("&c&l(!) &cThis enchantment book is already max success rate!"),
                CustomEnchants_Dust_CannotApplyArmor("&c&l(!) &cYou cannot apply dust to your armour!"),
                CustomEnchants_Dust_CannotMixRarities("&c&l(!) &cYou cannot mix different rarities of dust!"),
                CustomEnchants_Dust_Successful("&3You have successfully increased the percentage on your book."),
                CustomEnchants_Enchanted("&3&oThe item in your hand has been enchanted with &b{0} {1}&3."),
                CustomEnchants_EnchanterOpened("&3You are now opening the &bEnchanter&3."),
                CustomEnchants_EnchantLoreFormat("{0}{1} {2}"),
                CustomEnchants_EnchantNotSetup("&c&l(!) &cThe specified enchantment has not been set up! Please report this to an administrator."),
                CustomEnchants_ExpNotEnough("&c&l(!) &cYou do not have enough EXP to open that!"),
                CustomEnchants_LevelHigherThanMax("&c&l(!) &cThe specified level is greater then the max level."),
                CustomEnchants_LowerThanOne("&c&l(!) &cYou cannot make the enchant level lower than 1."),
                CustomEnchants_MaxEnchants("&c&l(!) &cYou cannot apply any more enchants to your item. You can only have &n{0}&c enchants on an item."),
                CustomEnchants_OrganisationScrollsDisabled("&c&lThis feature is currently disabled and is due to come in later in the season."),
                CustomEnchants_Tinkerer_Opened("&3You are now opening the &bTinkerer&3."),
                CustomEnchants_Tinkerer_OppositeSide("&c&l(!) &cYou cannot click on that side."),
                CustomEnchants_Tinkerer_Successful("&3You have successfully completed the transaction."),
                CustomEnchants_WhiteScrollLoreFormat("&3&lPROTECTED"),
                CustomEnchants_WhiteScroll_AlreadyHas("&c&l(!) &cThis itemstack is already protected!"),
                CustomEnchants_WhiteScroll_Purchased("&3You have purchased a whitescroll."),
                CustomEnchants_WhiteScroll_Successful("&3You have applied a whitescroll to this item."),

                GKit_Claimed("&e&l(&b&l!&e&l) &fYou have claimed your g-kit and it is now on cooldown."),
                GKit_Cooldown("&c&l(!) &cYou cannot use that kit for another &f{0}&c!"),
                GKit_NoAccess("&c&l(!) &cYou do not have access to that g-kit! However you can buy it at /buy ."),
                GKit_Reloaded("&e&l(&b&l!&e&l) &fYou have reloaded all the GKits data."),
                GKit_Reset_InvalidArgs("&c&l(!) &cInvalid args! &nUse /gkit reset [player]"),
                GKit_Reset_NeverJoined("&c&l(!) &cThe specified player has never joined before."),
                GKit_Reset_Sender("&e&l(&b&l!&e&l) &3You have reset &a{0}'s &3G-Kit timer(s)."),
                GKit_Reset_Receiver("&aYour G-Kit timer(s) have been reset.");


        private String path;
        private String msg;
        private static FileConfiguration LANG;

        Message(String path, String start) {
            this.path = path;
            this.msg = start;
        }

        Message(String string) {
            this.path = this.name().replace("_", ".");
            this.msg = string;
        }

    public static void setFile(FileConfiguration configuration) {
        LANG = configuration;
    }

    @Override
    public String toString() {
        return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, msg));
    }

    public String toString(Object... args) {
        String s = ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, msg));

        return getFinalized(s, args);
    }

    public String getDefault() {
        return this.msg;
    }

    public String getPath() {
        return this.path;
    }

    public void msg(CommandSender p, Object... order) {
        String s = toString();

        if(s.contains("\n")) {
            String[] split = s.split("\n");

            for(String inner : split) {
                sendMessage(p, inner, order);
            }
        } else {
            sendMessage(p, s, order);
        }
    }

    public void broadcast(Object... order) {
        String s = toString();

        if(s.contains("\n")) {
            String[] split = s.split("\n");

            for(String s1 : split) {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    sendMessage(player, s1, order);
                }
            }
        } else {
            for(Player player : Bukkit.getOnlinePlayers()) {
                sendMessage(player, s, order);
            }
        }
    }

    /**
     *
     * Private Enum methods
     *
     *
     */

    private String getFinalized(String string, Object... order) {
        int current = 0;

        for(Object object : order) {
            String placeholder = "{" + current + "}";

            if(string.contains(placeholder)) {
                if(object instanceof CommandSender) {
                    string = string.replace(placeholder, ((CommandSender) object).getName());
                }
                else if(object instanceof OfflinePlayer) {
                    string = string.replace(placeholder, ((OfflinePlayer) object).getName());
                }
                else if(object instanceof Location) {
                    Location location = (Location) object;
                    String repl = location.getWorld().getName() + ", " + location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ();

                    string = string.replace(placeholder, repl);
                }
                else if(object instanceof String) {
                    string = string.replace(placeholder, MessageUtils.translateString((String) object));
                }
                else if(object instanceof Double) {
                    string = string.replace(placeholder, ""+object);
                }
                else if(object instanceof Integer) {
                    string = string.replace(placeholder, ""+object);
                }
                else if(object instanceof ItemStack) {
                    string = string.replace(placeholder, getItemStackName((ItemStack) object));
                }
            }

            current++;
        }

        return string;
    }

    private void sendMessage(CommandSender target, String string, Object... order) {
        string = getFinalized(string, order);

        if(string.contains("{c}")) {
            MessageUtils.sendCenteredMessage(target, string);
        } else {
            target.sendMessage(string);
        }
    }

    private String getItemStackName(ItemStack itemStack) {
        String name = itemStack.getType().toString().replace("_", " ");

        if(itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName()) {
            return itemStack.getItemMeta().getDisplayName();
        }

        return name;
    }



}
