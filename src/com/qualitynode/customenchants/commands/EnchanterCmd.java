package com.qualitynode.customenchants.commands;

import com.qualitynode.customenchants.managers.GUIManager;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.builder.CommandService;
import net.aminecraftdev.utils.command.builder.attributes.Description;
import net.aminecraftdev.utils.command.builder.attributes.Name;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 22-Oct-17
 */
@Name("enchanter")
@Description("Purchase enchantment books from a GUI")
public class EnchanterCmd extends CommandService<Player> {

    public EnchanterCmd() {
        super(EnchanterCmd.class);
    }

    @Override
    public void execute(Player player, String[] args) {
        Message.CustomEnchants_EnchanterOpened.msg(player);
        GUIManager.openMainPanel(player);
    }
}
