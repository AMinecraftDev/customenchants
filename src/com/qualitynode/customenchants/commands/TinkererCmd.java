package com.qualitynode.customenchants.commands;

import com.qualitynode.customenchants.managers.TinkererManager;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.builder.CommandService;
import net.aminecraftdev.utils.command.builder.attributes.Description;
import net.aminecraftdev.utils.command.builder.attributes.Name;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 02-Aug-17
 */
@Name("tinkerer")
@Description("Exchange custom enchant gear and books for experience.")
public class TinkererCmd extends CommandService<Player> {

    public TinkererCmd() {
        super(TinkererCmd.class);
    }

    @Override
    public void execute(Player player, String[] args) {
        Message.CustomEnchants_Tinkerer_Opened.msg(player);
        TinkererManager.handleGUI(player);
    }
}
