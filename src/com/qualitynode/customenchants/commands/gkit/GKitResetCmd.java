package com.qualitynode.customenchants.commands.gkit;

import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.SubCommand;
import net.aminecraftdev.utils.core.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 21-Oct-17
 */
public class GKitResetCmd extends SubCommand {

    public GKitResetCmd() {
        super("reset");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(!commandSender.hasPermission("qn.admin")) return;

        if(args.length != 2) {
            Message.GKit_Reset_InvalidArgs.msg(commandSender);
            return;
        }

        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[1]);

        if(!offlinePlayer.hasPlayedBefore()) {
            Message.GKit_Reset_NeverJoined.msg(commandSender);
            return;
        }

        PlayerData.setPlayerData(offlinePlayer.getUniqueId(), "GKit", null);

        Message.GKit_Reset_Sender.msg(commandSender, args[1]);
        if(offlinePlayer.isOnline()) Message.GKit_Reset_Receiver.msg(offlinePlayer.getPlayer());
    }
}
