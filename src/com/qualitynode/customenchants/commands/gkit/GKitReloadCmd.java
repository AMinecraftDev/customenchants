package com.qualitynode.customenchants.commands.gkit;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.managers.GKitData;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.SubCommand;
import org.bukkit.command.CommandSender;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 21-Oct-17
 */
public class GKitReloadCmd extends SubCommand {

    private CustomEnchants plugin;
    private GKitData gKitData;

    public GKitReloadCmd(CustomEnchants plugin) {
        super("reload");

        this.plugin = plugin;
        this.gKitData = new GKitData();
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(!commandSender.hasPermission("qn.admin")) return;

        plugin.reloadFiles();
        gKitData.loadGKits();
        Message.GKit_Reloaded.msg(commandSender);
    }
}
