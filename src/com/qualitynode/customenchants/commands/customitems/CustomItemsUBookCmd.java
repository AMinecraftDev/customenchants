package com.qualitynode.customenchants.commands.customitems;

import com.qualitynode.customenchants.managers.EnchanterManager;
import com.qualitynode.customenchants.utils.Rarity;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.CommandUtils;
import net.aminecraftdev.utils.command.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 31-Oct-17
 *
 *    -1  0       1       2        3
 * /cusi ubook [tier] [player] [amount]
 */
public class CustomItemsUBookCmd  extends SubCommand {

    public CustomItemsUBookCmd() {
        super("ubook");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length != 4) {
            Message.CustomEnchants_CustomItems_Book_InvalidArgs.msg(sender);
            return;
        }

        Rarity tier = Rarity.getRarity(args[1]);
        Player target = CommandUtils.strAsPlayer(args[2]);
        Integer tempAmount = CommandUtils.getAmount(args[3]);
        int amount;

        if (target == null) {
            Message.NOT_ONLINE.msg(sender);
            return;
        }

        if(tier == null) {
            Message.CustomEnchants_CustomItems_UBook_InvalidRarity.msg(sender);
            return;
        }

        if(tempAmount == null) {
            Message.INVALID_INTEGER.msg(sender);
            return;
        }

        amount = tempAmount;

        ItemStack itemStack = EnchanterManager.getUnopenedBook(tier);

        for (int i = 0; i < amount; i++) {
            if (target.getInventory().firstEmpty() == -1) {
                target.getWorld().dropItemNaturally(target.getLocation(), itemStack);
            } else {
                target.getInventory().addItem(itemStack);
            }
        }

        Message.CustomEnchants_CustomItems_UBook_Given.msg(sender, target, amount, tier.name());
        Message.CustomEnchants_CustomItems_UBook_Received.msg(target, amount, tier.name());
    }
}