package com.qualitynode.customenchants.commands.customitems;

import com.qualitynode.customenchants.managers.DustManager;
import com.qualitynode.customenchants.utils.Rarity;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.CommandUtils;
import net.aminecraftdev.utils.command.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 *
 * /cusi dust (player) (rarity) (percentage) (amount)
 *   0    1      2        3          4           5
 */
public class CustomItemsDustCmd extends SubCommand {

    public CustomItemsDustCmd() {
        super("dust");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(args.length != 5) {
            Message.CustomEnchants_CustomItems_Dust_InvalidArgs.msg(commandSender);
            return;
        }

        Integer amount = CommandUtils.getAmount(args[4]);
        Integer percentage = CommandUtils.strAsInteger(args[3]);
        Player player = CommandUtils.strAsPlayer(args[1]);

        if(player == null) {
            Message.NOT_ONLINE.msg(commandSender);
            return;
        }

        if(amount == null) {
            Message.INVALID_INTEGER.msg(commandSender);
            return;
        }

        if(percentage == null) {
            Message.INVALID_INTEGER.msg(commandSender);
            return;
        }

        try {
            Rarity.valueOf(args[2]);
        } catch (IllegalArgumentException ex) {
            Message.CustomEnchants_CustomItems_Dust_RarityNotExist.msg(commandSender);
            return;
        }

        ItemStack itemStack = DustManager.getDust(Rarity.valueOf(args[2]), percentage, amount);

        Message.CustomEnchants_CustomItems_Dust_Given.msg(commandSender, player, amount, percentage, Rarity.valueOf(args[2]).name());
        Message.CustomEnchants_CustomItems_Dust_Received.msg(player, amount, percentage, Rarity.valueOf(args[2]).name());

        if(player.getInventory().firstEmpty() == -1) {
            player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
        } else {
            player.getInventory().addItem(itemStack);
        }
    }
}
