package com.qualitynode.customenchants.commands.customitems;

import com.qualitynode.customenchants.managers.WhiteScrollManager;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.CommandUtils;
import net.aminecraftdev.utils.command.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 *
 * /cusi whitescroll (player) (amount)
 *   0        1          2       3
 */
public class CustomItemsWhiteScrollCmd extends SubCommand {

    public CustomItemsWhiteScrollCmd() {
        super("whitescroll");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if(args.length != 3) {
            Message.CustomEnchants_CustomItems_WhiteScroll_InvalidArgs.msg(commandSender);
            return;
        }

        Integer amount = CommandUtils.getAmount(args[2]);
        Player player = CommandUtils.strAsPlayer(args[1]);

        if(player == null) {
            Message.NOT_ONLINE.msg(commandSender);
            return;
        }

        if(amount == null) {
            Message.INVALID_INTEGER.msg(commandSender);
            return;
        }

        ItemStack itemStack = WhiteScrollManager.getScroll(amount);

        Message.CustomEnchants_CustomItems_WhiteScroll_Given.msg(commandSender, player, amount);
        Message.CustomEnchants_CustomItems_WhiteScroll_Received.msg(player, amount);

        if(player.getInventory().firstEmpty() == -1) {
            player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
        } else {
            player.getInventory().addItem(itemStack);
        }
    }
}
