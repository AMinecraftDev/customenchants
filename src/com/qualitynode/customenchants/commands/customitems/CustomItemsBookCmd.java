package com.qualitynode.customenchants.commands.customitems;

import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.managers.EnchantManager;
import com.qualitynode.customenchants.managers.EnchanterManager;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.RandomUtils;
import net.aminecraftdev.utils.command.CommandUtils;
import net.aminecraftdev.utils.command.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 14-Aug-17
 *
 * /cusi book (player) (enchantment) (level) (amount) (success) (destroy)
 *   0    1      2          3           4       5         6        7
 */
public class CustomItemsBookCmd extends SubCommand {

    public CustomItemsBookCmd() {
        super("book");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length != 7) {
            Message.CustomEnchants_CustomItems_Book_InvalidArgs.msg(sender);
            return;
        }

        Player target = CommandUtils.strAsPlayer(args[1]);
        Integer tempLevel = CommandUtils.getAmount(args[3]);
        Integer tempAmount = CommandUtils.strAsInteger(args[4]);
        CEnchantment enchantment;
        int level;
        int amount;
        int success;
        int destroy;

        if(target == null) {
            Message.NOT_ONLINE.msg(sender);
            return;
        }

        if(!EnchantManager.getEnchants().containsKey(args[2].toUpperCase())) {
            Message.CustomEnchants_CustomItems_Book_EnchantNotExist.msg(sender);
            return;
        }

        enchantment = EnchantManager.getEnchants().get(args[2].toUpperCase());

        if(!enchantment.isEnabled()) {
            Message.CustomEnchants_CustomItems_Book_EnchantNotExist.msg(sender);
            return;
        }


        if(tempLevel == null) {
            Message.INVALID_INTEGER.msg(sender);
            return;
        }

        if(tempAmount == null) {
            Message.INVALID_INTEGER.msg(sender);
            return;
        }

        level = tempLevel;
        amount = tempAmount;

        if(!args[5].equalsIgnoreCase("?")) {
            Integer tempNumber = CommandUtils.strAsInteger(args[5]);

            if(tempNumber == null) {
                Message.INVALID_INTEGER.msg(sender);
                return;
            }

            success = tempNumber;
        } else {
            success = RandomUtils.getRandomInt(100) + 1;
        }

        if(!args[6].equalsIgnoreCase("?")) {
            Integer tempNumber = CommandUtils.strAsInteger(args[6]);

            if(tempNumber == null) {
                Message.INVALID_INTEGER.msg(sender);
                return;
            }

            destroy = tempNumber;
        } else {
            destroy = RandomUtils.getRandomInt(100) + 1;
        }

        ItemStack book = EnchanterManager.getOpenedBook(enchantment, level, success, destroy);

        for(int i = 0; i < amount; i++) {
            if(target.getInventory().firstEmpty() == -1) {
                target.getWorld().dropItemNaturally(target.getLocation(), book);
            } else {
                target.getInventory().addItem(book);
            }
        }

        Message.CustomEnchants_CustomItems_Book_Given.msg(sender, target, amount, enchantment.getName());
        Message.CustomEnchants_CustomItems_Book_Received.msg(target, amount, enchantment.getName());
    }
}
