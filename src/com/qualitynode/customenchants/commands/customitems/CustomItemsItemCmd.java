package com.qualitynode.customenchants.commands.customitems;

import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.managers.EnchantManager;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.NumberUtils;
import net.aminecraftdev.utils.RandomUtils;
import net.aminecraftdev.utils.command.SubCommand;
import net.aminecraftdev.utils.message.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 27-Aug-17
 *
 * /cusi item [itemtype] (player:name) (amount:amount) (name:name) (lore:lore[divided with /n]) (enchant:level)
 */
public class CustomItemsItemCmd extends SubCommand {

    public CustomItemsItemCmd() {
        super("item");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length < 2) {
            Message.CustomEnchants_CustomItems_InvalidArgs.msg(sender);
            return;
        }

        String itemType = args[1];
        Player target = null;
        String name = null;
        String lore = null;

        if(Material.getMaterial(itemType.toUpperCase()) == null) {
            Message.CustomEnchants_CustomItems_Item_MaterialNull.msg(sender);
            return;
        }

        ItemStack itemStack = new ItemStack(Material.getMaterial(itemType.toUpperCase()));
        ItemMeta itemMeta = itemStack.getItemMeta();

        if(args.length > 2) {
            for(int i = 1; i < args.length; i++) {
                String string = args[i];

                if(string.startsWith("player:")) {
                    if(target != null) continue;

                    String player = string.replace("player:", "");

                    if(Bukkit.getPlayer(player) == null) {
                        Message.CustomEnchants_CustomItems_Item_TargetOffline.msg(sender);
                        return;
                    }

                    target = Bukkit.getPlayer(player);
                }
                else if(string.startsWith("name:")) {
                    if(name != null) continue;

                    String innerName = MessageUtils.translateString(string.replace("name:", ""));

                    name = innerName;
                    itemMeta.setDisplayName(innerName);
                    itemStack.setItemMeta(itemMeta);
                }
                else if(string.startsWith("lore:")) {
                    if(lore != null) continue;

                    String replaced = MessageUtils.translateString(string.replace("lore:", "").replace("_", " "));
                    String[] array = replaced.split("/n");
                    List<String> innerLore = new ArrayList<>();

                    lore = replaced;
                    innerLore.addAll(Arrays.asList(array));
                    itemMeta.setLore(innerLore);
                    itemStack.setItemMeta(itemMeta);
                }
                else if(string.startsWith("amount:")) {
                    if(!NumberUtils.isStringInteger(string.replace("amount:", ""))) {
                        Message.INVALID_INTEGER.msg(sender);
                        return;
                    }

                    itemStack.setAmount(Integer.valueOf(string.replace("amount:", "")));
                }
                else {
                    int level = -1;
                    String enchant = "";

                    if(string.contains(":")) {
                        String[] split = string.split(":");

                        enchant = split[0];

                        if(NumberUtils.isStringInteger(split[1])) {
                            level = Integer.valueOf(split[1]);
                        }
                    }

                    if(EnchantManager.doesEnchantExist(enchant.toUpperCase())) {
                        CEnchantment enchantment = EnchantManager.getEnchants().get(enchant.toUpperCase());

                        if(level == -1) {
                            int maxLevel = enchantment.getMaxLevel();

                            level = RandomUtils.getRandomInt(maxLevel) + 1;
                        }

                        itemStack = EnchantManager.enchantItem(itemStack, enchantment, level, EnchantManager.getEnchantsAmount(itemStack));
                    } else {
                        if(Enchantment.getByName(enchant) != null) {
                            Enchantment enchantment = Enchantment.getByName(enchant);

                            if(level == -1) {
                                int maxLevel = enchantment.getMaxLevel();

                                level = RandomUtils.getRandomInt(maxLevel) + 1;
                            }

                            itemStack.addUnsafeEnchantment(enchantment, level);
                        }
                    }
                }
            }

            if(target == null) {
                if(!(sender instanceof Player)) {
                    Message.MUST_BE_PLAYER.msg(sender);
                    return;
                }

                target = (Player) sender;
            }

            target.getInventory().addItem(itemStack);
        }

    }
}
