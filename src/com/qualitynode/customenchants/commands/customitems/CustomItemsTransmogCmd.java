package com.qualitynode.customenchants.commands.customitems;

import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.SubCommand;
import org.bukkit.command.CommandSender;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 */
public class CustomItemsTransmogCmd extends SubCommand {

    public CustomItemsTransmogCmd() {
        super("transmog");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        Message.CustomEnchants_OrganisationScrollsDisabled.msg(commandSender);
    }
}
