package com.qualitynode.customenchants.commands;


import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.managers.EnchantManager;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.CommandUtils;
import net.aminecraftdev.utils.command.SubCommand;
import net.aminecraftdev.utils.command.SubCommandHandler;
import net.aminecraftdev.utils.command.builder.CommandService;
import net.aminecraftdev.utils.command.builder.attributes.Alias;
import net.aminecraftdev.utils.command.builder.attributes.Description;
import net.aminecraftdev.utils.command.builder.attributes.Name;
import net.aminecraftdev.utils.command.builder.attributes.Permission;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 02-Aug-17
 */
@Name("customitems")
@Alias({"cusi"})
@Permission("qn.admin")
@Description("Give yourself or another player custom items.")
public class CustomItemsCmd extends CommandService<CommandSender> implements SubCommandHandler {

    private List<SubCommand> subCommands = new ArrayList<>();

    public CustomItemsCmd() {
        super(CustomItemsCmd.class);
    }

    @Override
    public void addSubCommand(SubCommand subCommand) {
        subCommands.add(subCommand);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length == 0) {
            Message.CustomEnchants_CustomItems_InvalidArgs.msg(sender);
            return;
        }

        for(SubCommand subCommand : this.subCommands) {
            if(!subCommand.getSubCommand().equalsIgnoreCase(args[0])) continue;

            subCommand.execute(sender, args);
            return;
        }

        if(args.length == 2) {
            if(!(sender instanceof Player)) {
                Message.MUST_BE_PLAYER.msg(sender);
                return;
            }

            Player player = (Player) sender;
            CEnchantment enchantment;
            int level;

            if(!EnchantManager.doesEnchantExist(args[0])) {
                Message.CustomEnchants_CustomItems_EnchantNotExist.msg(sender);
                return;
            }

            enchantment = EnchantManager.getEnchants().get(args[0].toUpperCase());

            if(CommandUtils.strAsInteger(args[1]) == null) {
                Message.INVALID_INTEGER.msg(sender);
                return;
            }

            level = CommandUtils.strAsInteger(args[1]);
            EnchantManager.enchantItem(player, enchantment, level, true);
            return;
        }

        Message.CustomEnchants_CustomItems_InvalidArgs.msg(sender);
    }
}
