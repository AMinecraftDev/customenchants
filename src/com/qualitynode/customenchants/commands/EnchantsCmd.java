package com.qualitynode.customenchants.commands;

import com.qualitynode.customenchants.managers.GUIManager;
import com.qualitynode.customenchants.utils.Rarity;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.builder.CommandService;
import net.aminecraftdev.utils.command.builder.attributes.Alias;
import net.aminecraftdev.utils.command.builder.attributes.Description;
import net.aminecraftdev.utils.command.builder.attributes.Name;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 02-Aug-17
 */
@Alias({"ce", "ces"})
@Name("enchants")
@Description("View all available custom enchants in a GUI.")
public class EnchantsCmd extends CommandService<Player> {

    public EnchantsCmd() {
        super(EnchantsCmd.class);
    }

    @Override
    public void execute(Player player, String[] args) {
        Message.CustomEnchants_EnchanterOpened.msg(player);
        GUIManager.openEnchantsPage(player, Rarity.Common);
    }
}
