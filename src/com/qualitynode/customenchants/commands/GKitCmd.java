package com.qualitynode.customenchants.commands;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.managers.GKitData;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.command.SubCommand;
import net.aminecraftdev.utils.command.SubCommandHandler;
import net.aminecraftdev.utils.command.builder.CommandService;
import net.aminecraftdev.utils.command.builder.attributes.Alias;
import net.aminecraftdev.utils.command.builder.attributes.Description;
import net.aminecraftdev.utils.command.builder.attributes.Name;
import net.aminecraftdev.utils.inventory.Panel;
import net.aminecraftdev.utils.inventory.PanelBuilder;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 21-Oct-17
 */
@Name("gkit")
@Alias({"customkit", "ikit"})
@Description("View all the custom kits on the server.")
public class GKitCmd extends CommandService<CommandSender> implements SubCommandHandler {

    private List<SubCommand> subCommands = new ArrayList<>();
    private ConfigurationSection panel;
    private GKitData gKitData;

    public GKitCmd(CustomEnchants plugin) {
        super(GKitCmd.class);

        panel = plugin.getInventories().getConfigurationSection("GKitGUI");
        gKitData = new GKitData();
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                Message.MUST_BE_PLAYER.msg(sender);
                return;
            }

            openGUI((Player) sender);
            return;
        }

        for (SubCommand subCommand : this.subCommands) {
            if (args[0].equalsIgnoreCase(subCommand.getSubCommand())) {
                subCommand.execute(sender, args);
                return;
            }
        }

        if (!(sender instanceof Player)) {
            Message.MUST_BE_PLAYER.msg(sender);
            return;
        }

        openGUI((Player) sender);
        return;
    }

    public void addSubCommand(SubCommand subCommand) {
        this.subCommands.add(subCommand);
    }

    private void openGUI(Player player) {
        Panel panel = new PanelBuilder(this.panel).getPanel()
                .setCancelClick(true)
                .setDestroyWhenDone(true);

        gKitData.addToPanel(panel, player);

        panel.openFor(player);
    }
}
