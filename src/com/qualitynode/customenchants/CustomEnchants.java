package com.qualitynode.customenchants;

import com.qualitynode.customenchants.commands.*;
import com.qualitynode.customenchants.commands.customitems.*;
import com.qualitynode.customenchants.commands.gkit.GKitReloadCmd;
import com.qualitynode.customenchants.commands.gkit.GKitResetCmd;
import com.qualitynode.customenchants.enchants.PotionEnchants;
import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.handlers.GKit;
import com.qualitynode.customenchants.listeners.ApplyBookListener;
import com.qualitynode.customenchants.listeners.ApplyScrollListener;
import com.qualitynode.customenchants.listeners.ArmorListener;
import com.qualitynode.customenchants.listeners.EnchantListener;
import com.qualitynode.customenchants.managers.*;
import com.qualitynode.customenchants.utils.Rarity;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.file.types.YmlQFile;
import net.aminecraftdev.utils.qplugin.QPlugin;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 01-Aug-17
 */
public class CustomEnchants extends QPlugin {

    private YmlQFile enchants, gkits;

    @Override
    public void enable() {
        Rarity.setValues(this);
        new EnchanterManager(this);
        new TinkererManager(this);
        new DustManager(this);
        new BlackScrollManager(this);
        new WhiteScrollManager(this);
        EnchantManager enchantManager = new EnchantManager(this);
        GUIManager guiManager = new GUIManager(this);

        CEnchantment.setPlugin(this);
        enchantManager.loadEnchants();
        guiManager.refreshData().addHandlers();

        new GKit(this);
        new GKitData(this).loadGKits();
    }

    @Override
    public void disable() {

    }

    @Override
    protected void loadSQL() {}

    public FileConfiguration getEnchants() {
        return enchants.getConfig();
    }

    public FileConfiguration getGKits() {
        return gkits.getConfig();
    }

    @Override
    public void loadMessages() {
        for(Message message : Message.values()) {
            if(this.lang.getConfig().contains(message.getPath())) continue;

            this.lang.getConfig().set(message.getPath(), message.getDefault());
        }

        this.lang.saveFile();
        Message.setFile(this.lang.getConfig());
    }

    @Override
    public void loadFiles() {
        inventories = createYmlFile(new File(getDataFolder(), "inventories.yml"), true);
        enchants = createYmlFile(new File(getDataFolder(), "enchants.yml"), true);
        config = createYmlFile(new File(getDataFolder(), "config.yml"), true);
        gkits = createYmlFile(new File(getDataFolder(), "gkits.yml"), true);
    }

    @Override
    public void loadCommands() {
        CustomItemsCmd customItemsCmd = new CustomItemsCmd();

        customItemsCmd.addSubCommand(new CustomItemsBlackScrollCmd());
        customItemsCmd.addSubCommand(new CustomItemsDustCmd());
        customItemsCmd.addSubCommand(new CustomItemsTransmogCmd());
        customItemsCmd.addSubCommand(new CustomItemsWhiteScrollCmd());
        customItemsCmd.addSubCommand(new CustomItemsBookCmd());
        customItemsCmd.addSubCommand(new CustomItemsItemCmd());
        customItemsCmd.addSubCommand(new CustomItemsUBookCmd());

        GKitCmd gKitCmd = new GKitCmd(this);

        gKitCmd.addSubCommand(new GKitResetCmd());
        gKitCmd.addSubCommand(new GKitReloadCmd(this));

        addCommand(new EnchantsCmd());
        addCommand(new EnchanterCmd());
        addCommand(new TinkererCmd());
        addCommand(gKitCmd);
        addCommand(customItemsCmd);
    }

    @Override
    public void loadListeners() {
        addListener(new ApplyBookListener(this));
        addListener(new ApplyScrollListener(this));
        addListener(new ArmorListener(this));
        addListener(new EnchantListener());
        addListener(new PotionEnchants());
    }

    @Override
    public void saveFiles() {
        this.config.saveFile();
        this.enchants.saveFile();
        this.inventories.saveFile();
        this.gkits.saveFile();
        this.lang.saveFile();
    }

    public void reloadFiles() {
        this.config.reloadFile();
        this.enchants.reloadFile();
        this.inventories.reloadFile();
        this.gkits.reloadFile();
        this.lang.reloadFile();
    }
}
