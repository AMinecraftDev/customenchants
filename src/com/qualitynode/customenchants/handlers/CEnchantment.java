package com.qualitynode.customenchants.handlers;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.events.EnchantUseEvent;
import com.qualitynode.customenchants.managers.EnchantManager;
import com.qualitynode.customenchants.utils.EnchantmentType;
import com.qualitynode.customenchants.utils.Rarity;
import net.aminecraftdev.utils.ExpUtils;
import net.aminecraftdev.utils.RandomUtils;
import net.aminecraftdev.utils.dependencies.FactionHelper;
import net.aminecraftdev.utils.dependencies.WorldGuardHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 01-Aug-17
 */
public abstract class CEnchantment implements Listener {

    protected static CustomEnchants PLUGIN;
    private static List<String> disabledRegions = null;
    private static Map<Material, Material> oresMap = new HashMap<>();
    private static List<Material> fortunable = new ArrayList<>();
    private static List<PotionEffectType> badEffectTypes = new ArrayList<>();

    private String description;
    private int maxLevel;
    private double occurrence;
    private Rarity rarity;
    private boolean isEnabled;
    
    public CEnchantment() {
        if(disabledRegions == null) {
            disabledRegions = new ArrayList<>(PLUGIN.getConfig().getStringList("CustomEnchants.DisabledRegions"));
        }

        if(oresMap.isEmpty()) {
            oresMap.put(Material.COAL_ORE, Material.COAL);
            oresMap.put(Material.QUARTZ_ORE, Material.QUARTZ);
            oresMap.put(Material.IRON_ORE, Material.IRON_INGOT);
            oresMap.put(Material.GOLD_ORE, Material.GOLD_INGOT);
            oresMap.put(Material.DIAMOND_ORE, Material.DIAMOND);
            oresMap.put(Material.EMERALD_ORE, Material.EMERALD);
            oresMap.put(Material.REDSTONE_ORE, Material.REDSTONE);
            oresMap.put(Material.GLOWING_REDSTONE_ORE, Material.REDSTONE);
            oresMap.put(Material.LAPIS_ORE, new ItemStack(Material.INK_SACK, 1, (short) 4).getType());
        }

        if(fortunable.isEmpty()) {
            fortunable.add(Material.COAL);
            fortunable.add(Material.DIAMOND);
            fortunable.add(Material.EMERALD);
            fortunable.add(Material.REDSTONE);
            oresMap.put(Material.LAPIS_ORE, new ItemStack(Material.INK_SACK, 1, (short) 4).getType());
        }

        if(badEffectTypes.isEmpty()) {
            badEffectTypes.add(PotionEffectType.BLINDNESS);
            badEffectTypes.add(PotionEffectType.CONFUSION);
            badEffectTypes.add(PotionEffectType.HUNGER);
            badEffectTypes.add(PotionEffectType.POISON);
            badEffectTypes.add(PotionEffectType.SLOW);
            badEffectTypes.add(PotionEffectType.SLOW_DIGGING);
            badEffectTypes.add(PotionEffectType.WEAKNESS);
            badEffectTypes.add(PotionEffectType.WITHER);
        }

        String desc = "";

        for(String s : PLUGIN.getEnchants().getStringList(getName() + ".description")) {
            desc += s + "\n";
        }

        this.description = desc;
        this.maxLevel = PLUGIN.getEnchants().getInt(getName() + ".maxLevel");
        this.occurrence = PLUGIN.getEnchants().getDouble(getName() + ".occurrence");
        this.rarity = Rarity.valueOf(PLUGIN.getEnchants().getString(getName() + ".rarity"));
        this.isEnabled = PLUGIN.getEnchants().getBoolean(getName() + ".enabled");
    }

    public abstract String getName();

    public abstract EnchantmentType getEnchantmentType();

    public String getDescription() {
        return description;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public double getOccurrence() {
        return occurrence;
    }

    public Rarity getRarity() {
        return rarity;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public static void setPlugin(CustomEnchants plugin) {
        PLUGIN = plugin;
    }

    protected int getTotalExperience(Player player) {
        return ExpUtils.getTotalExperience(player);
    }

    protected void setTotalExperience(Player player, int exp) {
        ExpUtils.setTotalExperience(player, exp);
    }

    protected boolean hasEnchants(ItemStack itemStack) {
        return EnchantManager.hasEnchants(itemStack);
    }

    protected boolean hasEnchant(Player player, CEnchantment enchantment) {
        return EnchantManager.hasEnchant(player, enchantment);
    }

    protected boolean hasEnchant(ItemStack itemStack, CEnchantment enchantment) {
        return EnchantManager.hasEnchant(itemStack, enchantment);
    }

    protected boolean canEnchantItem(ItemStack itemStack, CEnchantment enchantment) {
        return EnchantManager.canEnchantItem(itemStack, enchantment);
    }

    protected int getEnchantLevel(Player player, CEnchantment enchantment) {
        return EnchantManager.getEnchantLevel(player, enchantment);
    }

    protected int getEnchantLevel(ItemStack itemStack, CEnchantment enchantment) {
        return EnchantManager.getEnchantLevel(itemStack, enchantment);
    }

    protected boolean canExecute(Player player, CEnchantment enchantment, int level) {
        return canExecute(player, player.getLocation(), enchantment, level);
    }

    protected boolean canExecute(Player player, Location location, CEnchantment enchantment, int level) {
        if(!canExecuteLocation(player, location)) return false;
        if(!canExecuteChance(level)) return false;

        EnchantUseEvent enchantUseEvent = new EnchantUseEvent(player, enchantment, level);
        Bukkit.getPluginManager().callEvent(enchantUseEvent);

        return !enchantUseEvent.isCancelled();
    }

    protected boolean canExecuteLocation(Player player, Location location) {
        List<String> currentRegions = WorldGuardHelper.getRegionNames(location);

        if(currentRegions == null) return false;

        for(String s : EnchantManager.getDisabledRegions()) {
            if(s == null) continue;
            if(currentRegions.contains(s)) return false;
        }

        try {
            if(getEnchantmentType() == EnchantmentType.Tool) {
                return FactionHelper.canFactionMine(player, location);
            } else {
                return FactionHelper.canPvP(location);
            }
        } catch (NoClassDefFoundError e) { return true; }
    }

    protected boolean canExecuteChance(int level) {
        double maxChance = getOccurrence();
        double perLevelChance = maxChance / getMaxLevel();
        double chance = perLevelChance * level;
        double randomChance = RandomUtils.getRandomInt(100);

        randomChance += RandomUtils.getRandom().nextDouble();

        return randomChance <= chance;
    }

    protected int getRandomNumber(int max) {
        return RandomUtils.getRandomInt(max) + 1;
    }

    protected void removeNegativePotionEffects(Player player) {
        for(PotionEffectType potionEffectType : badEffectTypes) {
            if(!player.hasPotionEffect(potionEffectType)) continue;

            player.removePotionEffect(potionEffectType);
        }
    }

    protected void removePotionEffect(Player player, PotionEffectType potionEffectType) {
        if(player.hasPotionEffect(potionEffectType)) {
            Collection<PotionEffect> collect = player.getActivePotionEffects();

            for(PotionEffect potionEffect : collect) {
                if(!potionEffect.getType().equals(potionEffectType)) continue;
                if(potionEffect.getDuration() < 10000) continue;

                player.removePotionEffect(potionEffectType);
                break;
            }
        }
    }

    protected List<LivingEntity> getNearbyEntities(double radius, Entity entity) {
        List<Entity> out = entity.getNearbyEntities(radius, radius, radius);
        List<LivingEntity> entities = new ArrayList<>();

        for(Entity inner : out) {
            if(!(inner instanceof LivingEntity)) continue;

            entities.add((LivingEntity) inner);
        }

        return entities;
    }

    protected boolean isPvPAllowed(Location location) {
        return WorldGuardHelper.isPvPAllowed(location);
    }

    protected Map<Material, Material> getOres() {
        return new HashMap<>(oresMap);
    }

}
