package com.qualitynode.customenchants.handlers;

import com.qualitynode.customenchants.enchants.common.*;
import com.qualitynode.customenchants.enchants.epic.*;
import com.qualitynode.customenchants.enchants.legendary.*;
import com.qualitynode.customenchants.enchants.mystical.*;
import com.qualitynode.customenchants.enchants.uncommon.*;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 01-Aug-17
 */
public enum EnchantHandler {

    //COMMON
    Beheading(new Beheading()),
    BurnShield(new BurnShield()),
    Crave(new Crave()),
    Decapitation(new Decapitation()),
    FeedMe(new FeedMe()),
    Gills(new Gills()),
    Knowledge(new Knowledge()),
    Oxygenate(new Oxygenate()),
    Pyrotechnic(new Pyrotechnic()),
    Stall(new Stall()),
    Torch(new Torch()),

    //EPIC
    Assassinate(new Assassinate()),
    AutoSmelt(new AutoSmelt()),
    Degenerating(new Degenerating()),
    Exhaust(new Exhaust()),
    Experience(new Experience()),
    Fortify(new Fortify()),
    Lifesteal(new Lifesteal()),

    //LEGENDARY
    Enlightened(new Enlightened()),
    Purification(new Purification()),
    Recoil(new Recoil()),
    Sonic(new Sonic()),
    Trenching(new Trenching()),

    //MYSTICAL
    DoubleTap(new DoubleTap()),
    Drunk(new Drunk()),
    Grapple(new Grapple()),
    HealthBoost(new HealthBoost()),
    MoonWalker(new MoonWalker()),
    NanoRepair(new NanoRepair()),
    PickPocket(new PickPocket()),
    Rekt(new Rekt()),

    //UNCOMMON
    HeadShot(new HeadShot()),
    LightWeight(new LightWeight()),
    Rabbit(new Rabbit()),
    Recover(new Recover()),
    Sturdy(new Sturdy()),
    Thor(new Thor());

    private CEnchantment enchantment;
    private String name;

    EnchantHandler(CEnchantment enchantment) {
        this.enchantment = enchantment;
        this.name = this.enchantment.getName();
    }

    public CEnchantment getEnchantment() {
        return this.enchantment;
    }

    public String getName() {
        return this.name;
    }

    public boolean isEnabled() {
        return getEnchantment().isEnabled();
    }
}
