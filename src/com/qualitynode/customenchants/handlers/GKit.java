package com.qualitynode.customenchants.handlers;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.core.PlayerData;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import net.aminecraftdev.utils.message.MessageUtils;
import net.aminecraftdev.utils.time.TimeUnit;
import net.aminecraftdev.utils.time.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 21-Oct-17
 */
public class GKit {

    private static boolean ENCHANTIFACCESS, ENCHANTIFNOACCESS, ENCHANTIFCOOLDOWN;
    private static List<String> EXTRALOREACCESS, EXTRALORENOACCESS, EXTRALORECOOLDOWN;

    private String gkitName;
    private ItemStack baseDisplayItem;
    private List<String> kit;
    private int cooldown;
    private String permission;
    private int slot;

    public GKit(CustomEnchants plugin) {
        ENCHANTIFACCESS = plugin.getConfig().getBoolean("GKit.Settings.enchanted.available");
        ENCHANTIFNOACCESS = plugin.getConfig().getBoolean("GKit.Settings.enchanted.noAccess");
        ENCHANTIFCOOLDOWN = plugin.getConfig().getBoolean("GKit.Settings.enchanted.cooldown");

        EXTRALOREACCESS = plugin.getConfig().getStringList("GKit.Settings.loreAddons.available");
        EXTRALORECOOLDOWN = plugin.getConfig().getStringList("GKit.Settings.loreAddons.cooldown");
        EXTRALORENOACCESS = plugin.getConfig().getStringList("GKit.Settings.loreAddons.noAccess");
    }

    public GKit(ConfigurationSection gkitSection) {
        this.gkitName = gkitSection.getName();
        this.baseDisplayItem = ItemStackUtils.createItemStack(gkitSection.getConfigurationSection("Item"));
        this.cooldown = (int) TimeUnit.SECONDS.to(TimeUnit.MILLISECONDS, gkitSection.getInt("cooldown"));
        this.permission = gkitSection.getString("permission");
        this.kit = gkitSection.getStringList("Kit");
        this.slot = gkitSection.getInt("slot");
    }

    /**
     * GET INFO BASED ON G-KIT
     */
    public String getGKitName() {
        return gkitName;
    }

    public int getSlot() {
        return slot;
    }

    public int getCooldown() {
        return cooldown;
    }

    /**
     * PRE-OPENING OF GKITS
     */
    public boolean hasPermission(Player player) {
        return player.hasPermission(this.permission);
    }

    public boolean canPlayerUseKit(Player player) {
        if(PlayerData.getPlayerData(player.getUniqueId()) == null) return true;

        ConfigurationSection configurationSection = PlayerData.getPlayerData(player.getUniqueId());
        long availableNext = configurationSection.getLong("GKit." + this.gkitName, 0L);

        if(!configurationSection.contains("GKit." + this.gkitName)) return true;

        return System.currentTimeMillis() > availableNext;
    }

    /**
     * HANDLE G-KIT RECEIVING
     */
    public void givePlayerKit(Player player) {
        for(String s : this.kit) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s.replace("{player}", player.getName()));
        }
    }

    public void applyCooldown(Player player) {
        PlayerData.setPlayerData(player.getUniqueId(), "GKit." + this.gkitName, System.currentTimeMillis() + this.cooldown);
    }

    public void sendCooldownMessage(Player player) {
        if(!PlayerData.getPlayerData(player.getUniqueId()).contains("GKit." + this.gkitName)) return;

        Message.GKit_Cooldown.msg(player, getCooldown(player));
    }

    /**
     * HANDLE G-KIT GUI
     */
    public ItemStack getItemStack(Player player) {
        ItemStack itemStack = baseDisplayItem.clone();
        ItemMeta itemMeta = itemStack.getItemMeta();

        if(!player.hasPermission(this.permission)) {
            itemStack.setItemMeta(applyExtraLore(itemMeta, EXTRALORENOACCESS));

            return ENCHANTIFNOACCESS? ItemStackUtils.addGlow(itemStack) : itemStack;
        }

        if(!canPlayerUseKit(player)) {
            itemStack.setItemMeta(applyCooldownExtraLore(itemMeta, EXTRALORECOOLDOWN, getCooldown(player)));

            return ENCHANTIFCOOLDOWN? ItemStackUtils.addGlow(itemStack) : itemStack;
        } else {
            itemStack.setItemMeta(applyExtraLore(itemMeta, EXTRALOREACCESS));

            return ENCHANTIFACCESS? ItemStackUtils.addGlow(itemStack) : itemStack;
        }

    }

    private ItemMeta applyCooldownExtraLore(ItemMeta itemMeta, List<String> extraLore, String cooldown) {
        List<String> overridenLore = new ArrayList<>();

        for(String s : extraLore) {
            overridenLore.add(s.contains("{cooldown}")? s.replace("{cooldown}", cooldown) : s);
        }

        return applyExtraLore(itemMeta, overridenLore);
    }


    private ItemMeta applyExtraLore(ItemMeta itemMeta, List<String> extraLore) {
        List<String> newLore = new ArrayList<>();

        newLore.addAll(itemMeta.getLore());

        for(String s : extraLore) {
            newLore.add(MessageUtils.translateString(s));
        }

        itemMeta.setLore(newLore);
        return itemMeta;
    }

    /**
     * HANDLE COOLDOWN MESSAGE
     */
    private String getCooldown(Player player) {
        int availableNext = PlayerData.getPlayerData(player.getUniqueId()).getInt("GKit." + this.gkitName);
        long timeLeft = availableNext - System.currentTimeMillis();

        return TimeUtil.getFormattedTime(TimeUnit.MILLISECONDS, (int) timeLeft > 0? (int) timeLeft : 0);
    }


}
