package com.qualitynode.customenchants.handlers.types.armour;

import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.utils.EnchantmentType;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 13-Aug-17
 */
public abstract class LeggingsEnchantment extends CEnchantment {

    @Override
    public EnchantmentType getEnchantmentType() {
        return EnchantmentType.Leggings;
    }

}
