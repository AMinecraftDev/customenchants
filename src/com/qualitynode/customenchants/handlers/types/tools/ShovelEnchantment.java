package com.qualitynode.customenchants.handlers.types.tools;

import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.utils.EnchantmentType;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 13-Aug-17
 */
public abstract class ShovelEnchantment extends CEnchantment {

    @Override
    public EnchantmentType getEnchantmentType() {
        return EnchantmentType.Shovel;
    }

}
