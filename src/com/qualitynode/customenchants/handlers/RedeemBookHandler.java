package com.qualitynode.customenchants.handlers;

import com.qualitynode.customenchants.managers.EnchanterManager;
import com.qualitynode.customenchants.utils.Rarity;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.PluginUtils;
import net.aminecraftdev.utils.voucher.base.VoucherClickAction;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 20-Aug-17
 */
public class RedeemBookHandler implements VoucherClickAction {

    @Override
    public boolean onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        ItemStack clone = PluginUtils.getItemInHand(player).clone();
        Rarity rarity = EnchanterManager.getRarity(clone);

        clone.setAmount(1);

        if(rarity == null) return false;
        if(EnchanterManager.getRandomEnchant(rarity) == null) {
            Message.CustomEnchants_EnchantNotSetup.msg(player);
            return false;
        }

        CEnchantment enchantment = EnchanterManager.getRandomEnchant(rarity);
        int level = EnchanterManager.getRandomLevel(enchantment);
        ItemStack book = EnchanterManager.getOpenedBook(enchantment, level);

        if(player.getInventory().firstEmpty() == -1) {
            player.getWorld().dropItemNaturally(player.getLocation(), book);
            Message.INVENTORY_SPACE.msg(player);
        } else {
            player.getInventory().addItem(book);
        }

        Message.CustomEnchants_Book_Revealed.msg(player);
        return true;
    }
}
