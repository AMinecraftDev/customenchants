package com.qualitynode.customenchants.managers;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.handlers.EnchantHandler;
import com.qualitynode.customenchants.utils.EnchantmentType;
import com.qualitynode.customenchants.utils.Rarity;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.PotionUtils;
import net.aminecraftdev.utils.factory.NbtFactory;
import net.aminecraftdev.utils.message.RomanNumber;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 02-Aug-17
 */
public class EnchantManager {

    private static final Map<String, CEnchantment> ENCHANTS = new HashMap<>();
    private static final Map<Rarity, List<CEnchantment>> RARITY_ENCHANTS = new HashMap<>();
    private static final List<String> DISABLED_REGIONS = new ArrayList<>();
    private static final String NBT_ENCH_AMOUNT = "EnchantAmount";
    private static final String NBT_ENCH_PATH = "Enchant.";

    private static CustomEnchants PLUGIN;

    public EnchantManager(CustomEnchants plugin) {
        PLUGIN = plugin;
        DISABLED_REGIONS.clear();
        DISABLED_REGIONS.addAll(PLUGIN.getConfig().getStringList("CustomEnchants.DisabledRegions"));
    }

    public void loadEnchants() {
        PluginManager pluginManager = Bukkit.getPluginManager();

        for(EnchantHandler enchantHandler : EnchantHandler.values()) {
            CEnchantment enchantment = enchantHandler.getEnchantment();
            Rarity rarity = enchantment.getRarity();

            if(!enchantHandler.isEnabled()) {
                PLUGIN.log(" > &cEnchant '" + rarity.getPrefix() + enchantment.getName() + "' &chas been disabled.");
                continue;
            }

            List<CEnchantment> enchantsToRarity = getRarityEnchants().getOrDefault(rarity, new ArrayList<>());

            ENCHANTS.put(enchantHandler.getName().toUpperCase(), enchantment);
            pluginManager.registerEvents(enchantment, PLUGIN);
            enchantsToRarity.add(enchantment);
            RARITY_ENCHANTS.put(rarity, enchantsToRarity);

            PLUGIN.log(" > &aEnchant " + rarity.getPrefix() + enchantment.getName() + "&a has been enabled.");
        }
    }

    public static String getNbtEnchAmount() {
        return NBT_ENCH_AMOUNT;
    }

    public static String getNBTEnchantPath() {
        return NBT_ENCH_PATH;
    }

    public static Map<String, CEnchantment> getEnchants() {
        return ENCHANTS;
    }

    public static Map<Rarity, List<CEnchantment>> getRarityEnchants() {
        return RARITY_ENCHANTS;
    }

    public static List<String> getDisabledRegions() {
        return DISABLED_REGIONS;
    }

    public static boolean doesEnchantExist(String enchant) {
        for(String name : ENCHANTS.keySet()) {
            if(enchant.equalsIgnoreCase(name)) return true;
        }

        return false;
    }

    public static int getMaxEnchantsAllowed(Player player) {
        ConfigurationSection configurationSection = PLUGIN.getConfig().getConfigurationSection("CustomEnchants.MaxEnchants");
        int max = 0;

        for(String s : configurationSection.getKeys(false)) {
            int amount = Integer.valueOf(s);
            String permission = configurationSection.getString(s + ".permission");

            if(!player.hasPermission(permission)) continue;
            if(amount < max) continue;

            max = amount;
        }

        return max;
    }

    public static boolean canApplyScroll(ItemStack itemStack) {
        return EnchantmentType.All.doesMaterialEndWith(itemStack.getType());
    }

    public static boolean canEnchantItem(ItemStack itemStack, CEnchantment enchantment) {
        if(itemStack == null || itemStack.getType() == Material.AIR) return false;

        return enchantment.getEnchantmentType().doesMaterialEndWith(itemStack.getType()) && itemStack.getAmount() == 1;
    }

    public static boolean hasEnchants(ItemStack itemStack) {
        if(itemStack == null || itemStack.getType() == Material.AIR) return false;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        for(String s : compound.keySet()) {
            if(s.startsWith(NBT_ENCH_PATH)) return true;
        }

        return false;
    }

    public static boolean hasEnchant(Player player, CEnchantment enchantment) {
        for(ItemStack itemStack : player.getInventory().getArmorContents()) {
            if(hasEnchant(itemStack, enchantment)) return true;
        }

        return hasEnchant(player.getItemInHand(), enchantment);
    }

    public static boolean hasEnchant(ItemStack itemStack, CEnchantment enchantment) {
        if(itemStack == null || itemStack.getType() == Material.AIR || !enchantment.getEnchantmentType().doesMaterialEndWith(itemStack.getType())) return false;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.containsKey(NBT_ENCH_PATH + enchantment.getName());
    }

    public static int getEnchantsAmount(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.getInteger(NBT_ENCH_AMOUNT, 0);
    }

    public static int getEnchantLevel(Player player, CEnchantment enchantment) {
        int max = 0;

        for(ItemStack itemStack : player.getInventory().getArmorContents()) {
            if(!hasEnchant(itemStack, enchantment)) continue;
            if(max > getEnchantLevel(itemStack, enchantment)) continue;

            max = getEnchantLevel(itemStack, enchantment);
        }

        int itemInHand = getEnchantLevel(player.getItemInHand(), enchantment);

        if(max < itemInHand) max = itemInHand;
        return max;
    }

    public static int getEnchantLevel(ItemStack itemStack, CEnchantment enchantment) {
        if(itemStack == null || itemStack.getType() == Material.AIR || !enchantment.getEnchantmentType().doesMaterialEndWith(itemStack.getType())) return -1;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.containsKey(NBT_ENCH_PATH + enchantment.getName()) ? compound.getInteger(NBT_ENCH_PATH + enchantment.getName(), -1) : -1;
    }

    public static ItemStack enchantItem(Player player, CEnchantment enchantment, int level, boolean ignoreMax) {
        ItemStack itemStack = player.getItemInHand();

        if(!canEnchantItem(itemStack, enchantment)) {
            Message.CustomEnchants_CannotEnchant.msg(player);
            return itemStack;
        }

        if(level < 1) {
            Message.CustomEnchants_LowerThanOne.msg(player);
            return itemStack;
        }

        int currentEnchants = getEnchantsAmount(itemStack);

        if(!ignoreMax) {
            if(level > enchantment.getMaxLevel()) {
                Message.CustomEnchants_LevelHigherThanMax.msg(player);
                return itemStack;
            }

            if(currentEnchants >= getMaxEnchantsAllowed(player)) {
                Message.CustomEnchants_MaxEnchants.msg(player, getMaxEnchantsAllowed(player));
                return itemStack;
            }
        }

        Message.CustomEnchants_Enchanted.msg(player, enchantment.getName(), level);
        return enchantItem(itemStack, enchantment, level, currentEnchants);
    }

    public static ItemStack enchantItem(ItemStack itemStack, CEnchantment enchantment, int level, int currentEnchants) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        if(!compound.containsKey(NBT_ENCH_PATH + enchantment.getName())) {
            compound.put(NBT_ENCH_AMOUNT, (currentEnchants + 1));
        }

        compound.put(NBT_ENCH_PATH + enchantment.getName(), level);

        ItemMeta itemMeta = craftStack.getItemMeta();

        craftStack.setItemMeta(updateItemMeta(itemMeta, enchantment, level));
        return craftStack;
    }



    public static Map<CEnchantment, Map<PotionEffectType, Integer>> getEnchantmentPotions() {
        Map<CEnchantment, Map<PotionEffectType, Integer>> enchants = new HashMap<>();

        enchants.put(EnchantHandler.BurnShield.getEnchantment(), new HashMap<>());
        enchants.get(EnchantHandler.BurnShield.getEnchantment()).put(PotionEffectType.FIRE_RESISTANCE, -1);

        enchants.put(EnchantHandler.Gills.getEnchantment(), new HashMap<>());
        enchants.get(EnchantHandler.Gills.getEnchantment()).put(PotionEffectType.WATER_BREATHING, -1);

        enchants.put(EnchantHandler.Torch.getEnchantment(), new HashMap<>());
        enchants.get(EnchantHandler.Torch.getEnchantment()).put(PotionUtils.getGlowing(), -1);

        enchants.put(EnchantHandler.Sonic.getEnchantment(), new HashMap<>());
        enchants.get(EnchantHandler.Sonic.getEnchantment()).put(PotionEffectType.SPEED, -1);

        enchants.put(EnchantHandler.Drunk.getEnchantment(), new HashMap<>());
        enchants.get(EnchantHandler.Drunk.getEnchantment()).put(PotionEffectType.INCREASE_DAMAGE, -1);
        enchants.get(EnchantHandler.Drunk.getEnchantment()).put(PotionEffectType.DAMAGE_RESISTANCE, -1);
        enchants.get(EnchantHandler.Drunk.getEnchantment()).put(PotionEffectType.SLOW_DIGGING, -1);
        enchants.get(EnchantHandler.Drunk.getEnchantment()).put(PotionEffectType.SLOW, 0);

        enchants.put(EnchantHandler.MoonWalker.getEnchantment(), new HashMap<>());
        enchants.get(EnchantHandler.MoonWalker.getEnchantment()).put(PotionEffectType.JUMP, 1);

        enchants.put(EnchantHandler.Rabbit.getEnchantment(), new HashMap<>());
        enchants.get(EnchantHandler.Rabbit.getEnchantment()).put(PotionEffectType.JUMP, -1);

        enchants.put(EnchantHandler.Sturdy.getEnchantment(), new HashMap<>());
        enchants.get(EnchantHandler.Sturdy.getEnchantment()).put(PotionEffectType.DAMAGE_RESISTANCE, -1);

        enchants.put(EnchantHandler.HealthBoost.getEnchantment(), new HashMap<>());
        enchants.get(EnchantHandler.HealthBoost.getEnchantment()).put(PotionEffectType.HEALTH_BOOST, -1);

        return enchants;
    }

    public static Map<PotionEffectType, Integer> getUpdatedEffects(Player player, ItemStack include, ItemStack exlude, CEnchantment enchantment) {
        Map<PotionEffectType, Integer> effects = new HashMap<>();
        List<ItemStack> itemStacks = new ArrayList<>();

        itemStacks.addAll(Arrays.asList(player.getEquipment().getArmorContents()));

        if(include == null) include = new ItemStack(Material.AIR);
        if(exlude == null) exlude = new ItemStack(Material.AIR);
        if(exlude.isSimilar(include)) exlude = new ItemStack(Material.AIR);

        itemStacks.add(include);

        for(CEnchantment ench : getEnchantmentPotions().keySet()) {
            for(ItemStack armour : itemStacks) {
                if(armour == null) continue;
                if(armour.isSimilar(exlude)) continue;
                if(!hasEnchant(armour, ench)) continue;

                int power = getEnchantLevel(armour, ench);

                for(PotionEffectType type : getEnchantmentPotions().get(ench).keySet()) {
                    if(!getEnchantmentPotions().get(ench).containsKey(type)) continue;

                    if(effects.containsKey(type)) {
                        int updated = effects.get(type);

                        if(updated < (power + getEnchantmentPotions().get(ench).get(type))) {
                            if(type == PotionEffectType.DAMAGE_RESISTANCE || type == PotionEffectType.SLOW) {
                                if(power <= 2) {
                                    effects.put(type, 0);
                                } else {
                                    effects.put(type, 1);
                                }
                            } else {
                                effects.put(type, power + getEnchantmentPotions().get(ench).get(type));
                            }
                        }
                    } else {
                        if(type == PotionEffectType.DAMAGE_RESISTANCE || type == PotionEffectType.SLOW) {
                            if(power <= 2) {
                                effects.put(type, 0);
                            } else {
                                effects.put(type, 1);
                            }
                        } else {
                            effects.put(type, power + getEnchantmentPotions().get(ench).get(type));
                        }
                    }
                }
            }
        }

        for(PotionEffectType type : getEnchantmentPotions().get(enchantment).keySet()) {
            if(!effects.containsKey(type)) {
                effects.put(type, -1);
            }
        }

        return effects;
    }

    public static ItemMeta removeEnchantMeta(ItemMeta itemMeta, CEnchantment enchantment, int level) {
        List<String> lore = new ArrayList<>();
        String checkFor = Message.CustomEnchants_EnchantLoreFormat.toString(enchantment.getRarity().getPrefix(), enchantment.getName(), RomanNumber.toRoman(level));

        if(itemMeta.hasLore()) {
            for(String s : itemMeta.getLore()) {
                if(s.equals(checkFor)) continue;

                lore.add(s);
            }
        }

        itemMeta.setLore(lore);
        return itemMeta;
    }

    private static ItemMeta updateItemMeta(ItemMeta itemMeta, CEnchantment enchantment, int level) {
        List<String> lore = new ArrayList<>();
        String checkFor = Message.CustomEnchants_EnchantLoreFormat.toString(enchantment.getRarity().getPrefix(), enchantment.getName(), "");

        lore.add(Message.CustomEnchants_EnchantLoreFormat.toString(enchantment.getRarity().getPrefix(), enchantment.getName(), RomanNumber.toRoman(level)));

        if(itemMeta.hasLore()) {
            for(String s : itemMeta.getLore()) {
                if(s.startsWith(checkFor)) continue;

                lore.add(s);
            }
        }

        itemMeta.setLore(lore);
        return itemMeta;
    }

}
