package com.qualitynode.customenchants.managers;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.utils.Rarity;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.NumberUtils;
import net.aminecraftdev.utils.dependencies.VaultHelper;
import net.aminecraftdev.utils.inventory.Panel;
import net.aminecraftdev.utils.inventory.PanelBuilder;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import net.aminecraftdev.utils.message.RomanNumber;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 08-Aug-17
 */
public class GUIManager {

    private static final Map<Integer, Rarity> RARITY_ENCHANTER_SLOTS = new HashMap<>();
    private static final List<Integer> ORGANISATIONAL_SCROLL_SLOTS = new ArrayList<>();
    private static final List<Integer> PROTECTION_SCROLL_SLOTS = new ArrayList<>();
    private static final Map<Rarity, Panel> ENCHANTS_PANELS = new HashMap<>();
    private static final List<Integer> BLACK_SCROLL_SLOTS = new ArrayList<>();
    private static final Map<Integer, Rarity> RARITY_SLOTS = new HashMap<>();
    private static final List<Integer> ENCHANTINFO_SLOTS = new ArrayList<>();
    private static final List<Integer> TINKERER_SLOTS = new ArrayList<>();

    private static ConfigurationSection mainConfigSection, enchantsConfigSection, enchantGemConfigSection;
    private static int oScrollCost, wScrollCost, bScrollCost;
    private static String enchanterMenuColor;
    private static Panel mainPanel;


    public GUIManager(CustomEnchants plugin) {
        mainConfigSection = plugin.getInventories().getConfigurationSection("CustomEnchants.MainGUI");
        enchantsConfigSection = plugin.getInventories().getConfigurationSection("CustomEnchants.EnchantsGUI");
        enchantGemConfigSection = plugin.getConfig().getConfigurationSection("CustomEnchants.Items.EnchantsGem");
        oScrollCost = plugin.getConfig().getInt("CustomEnchants.Enchanter.Costs.OrganisationalScroll");
        wScrollCost = plugin.getConfig().getInt("CustomEnchants.Enchanter.Costs.ProtectionScroll");
        bScrollCost = plugin.getConfig().getInt("CustomEnchants.Enchanter.Costs.BlackScroll");
        enchanterMenuColor = plugin.getConfig().getString("CustomEnchants.Colours.enchantsMenu", "&7&o");
    }

    public GUIManager refreshData() {
        ORGANISATIONAL_SCROLL_SLOTS.clear();
        PROTECTION_SCROLL_SLOTS.clear();
        RARITY_ENCHANTER_SLOTS.clear();
        BLACK_SCROLL_SLOTS.clear();
        ENCHANTINFO_SLOTS.clear();
        ENCHANTS_PANELS.clear();
        TINKERER_SLOTS.clear();
        RARITY_SLOTS.clear();

        ConfigurationSection mainItemSection = mainConfigSection.getConfigurationSection("Items");
        ConfigurationSection enchantsItemSection = enchantsConfigSection.getConfigurationSection("Items");

        for(String s : enchantsItemSection.getKeys(false)) {
            ConfigurationSection innerSection = enchantsItemSection.getConfigurationSection(s);
            int slot = Integer.valueOf(s) - 1;

            if(innerSection.contains("Common") && innerSection.getBoolean("Common")) {
                RARITY_SLOTS.put(slot, Rarity.Common);
            } else if(innerSection.contains("Uncommon") && innerSection.getBoolean("Uncommon")) {
                RARITY_SLOTS.put(slot, Rarity.Uncommon);
            } else if(innerSection.contains("Epic") && innerSection.getBoolean("Epic")) {
                RARITY_SLOTS.put(slot, Rarity.Epic);
            } else if(innerSection.contains("Legendary") && innerSection.getBoolean("Legendary")) {
                RARITY_SLOTS.put(slot, Rarity.Legendary);
            } else if(innerSection.contains("Mystical") && innerSection.getBoolean("Mystical")) {
                RARITY_SLOTS.put(slot, Rarity.Mystical);
            }
        }

        for(String s : mainItemSection.getKeys(false)) {
            ConfigurationSection innerSection = mainItemSection.getConfigurationSection(s);
            int slot = Integer.valueOf(s) - 1;

            if(innerSection.contains("EnchantInfo") && innerSection.getBoolean("EnchantInfo")) {
                ENCHANTINFO_SLOTS.add(slot);
            } else if(innerSection.contains("TScroll") && innerSection.getBoolean("TScroll")) {
                ORGANISATIONAL_SCROLL_SLOTS.add(slot);
            } else if(innerSection.contains("Tinkerer") && innerSection.getBoolean("Tinkerer")) {
                TINKERER_SLOTS.add(slot);
            } else if(innerSection.contains("WScroll") && innerSection.getBoolean("WScroll")) {
                PROTECTION_SCROLL_SLOTS.add(slot);
            } else if(innerSection.contains("BScroll") && innerSection.getBoolean("BScroll")) {
                BLACK_SCROLL_SLOTS.add(slot);
            } else if(innerSection.contains("Common") && innerSection.getBoolean("Common")) {
                RARITY_ENCHANTER_SLOTS.put(slot, Rarity.Common);
            } else if(innerSection.contains("Uncommon") && innerSection.getBoolean("Uncommon")) {
                RARITY_ENCHANTER_SLOTS.put(slot, Rarity.Uncommon);
            } else if(innerSection.contains("Epic") && innerSection.getBoolean("Epic")) {
                RARITY_ENCHANTER_SLOTS.put(slot, Rarity.Epic);
            } else if(innerSection.contains("Legendary") && innerSection.getBoolean("Legendary")) {
                RARITY_ENCHANTER_SLOTS.put(slot, Rarity.Legendary);
            } else if(innerSection.contains("Mystical") && innerSection.getBoolean("Mystical")) {
                RARITY_ENCHANTER_SLOTS.put(slot, Rarity.Mystical);
            }
        }

        for(Rarity rarity : Rarity.values()) {
            Panel panel = new PanelBuilder(enchantsConfigSection).getPanel()
                    .setCancelClick(true)
                    .setDestroyWhenDone(false);

            for(CEnchantment enchantment : EnchantManager.getRarityEnchants().get(rarity)) {
                if(!enchantment.isEnabled()) continue;
                if(enchantment.getRarity() != rarity) continue;

                Map<String, String> replace = new HashMap<>();

                replace.put("{enchantColor}", enchantment.getRarity().getPrefix());
                replace.put("{enchantName}", enchantment.getName());
                replace.put("{enchantMax}", RomanNumber.toRoman(enchantment.getMaxLevel()));
                replace.put("{description}", enchantment.getDescription().replace("{c}", enchanterMenuColor));
                replace.put("{enchantType}", enchantment.getEnchantmentType().toString());

                ItemStack itemStack = ItemStackUtils.createItemStack(enchantGemConfigSection, 1, replace);

                panel.addItem(itemStack);
            }

            for(Map.Entry<Integer, Rarity> entry : RARITY_SLOTS.entrySet()) {
                int slot = entry.getKey();
                Rarity rarityAtSlot = entry.getValue();

                panel.setOnClick(slot, event -> {
                    Player player = (Player) event.getWhoClicked();

                    openEnchantsPage(player, rarityAtSlot);
                });
            }

            ENCHANTS_PANELS.put(rarity, panel);
        }

        mainPanel = new PanelBuilder(mainConfigSection).getPanel()
                .setCancelClick(true)
                .setDestroyWhenDone(false);
        return this;
    }

    public void addHandlers() {
        for(Map.Entry<Integer, Rarity> entry : RARITY_ENCHANTER_SLOTS.entrySet()) {
            int slot = entry.getKey();
            Rarity rarity = entry.getValue();

            mainPanel.setItem(slot, EnchanterManager.getEnchanterBook(rarity), event -> {
                Player player = (Player) event.getWhoClicked();

                EnchanterManager.runEnchantCheck(player, rarity);
            });
        }

        for(int i : ENCHANTINFO_SLOTS) {
            mainPanel.setOnClick(i, event -> {
                Player player = (Player) event.getWhoClicked();

                openEnchantsPage(player, Rarity.Common);
            });
        }

        for(int i : TINKERER_SLOTS) {
            mainPanel.setOnClick(i, event -> {
                Player player = (Player) event.getWhoClicked();

                TinkererManager.handleGUI(player);
            });
        }

        for(int i : BLACK_SCROLL_SLOTS) {
            mainPanel.setOnClick(i, event -> {
                Player player = (Player) event.getWhoClicked();

                attemptPurchaseBScroll(player);
            });
        }

        for(int i : PROTECTION_SCROLL_SLOTS) {
            mainPanel.setOnClick(i, event -> {
                Player player = (Player) event.getWhoClicked();

                attemptPurchasePScroll(player);
            });
        }

        for(int i : ORGANISATIONAL_SCROLL_SLOTS) {
            mainPanel.setOnClick(i, event -> {
                Player player = (Player) event.getWhoClicked();

                attemptPurchaseOScroll(player);
            });
        }
    }

    public static void openMainPanel(Player player) {
        mainPanel.openFor(player);
    }

    public static void openEnchantsPage(Player player, Rarity rarity) {
        ENCHANTS_PANELS.get(rarity).openFor(player);
    }

    private void attemptPurchasePScroll(Player player) {
        double balance = VaultHelper.getEconomy().getBalance(player);

        if(balance < wScrollCost) {
            Message.INSUFFICIENT_FUNDS.msg(player, NumberUtils.formatDouble(wScrollCost - balance));
            return;
        }

        ItemStack itemStack = WhiteScrollManager.getScroll(1);

        if(player.getInventory().firstEmpty() == -1) {
            player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
        } else {
            player.getInventory().addItem(itemStack);
        }

        VaultHelper.getEconomy().withdrawPlayer(player, wScrollCost);
        Message.CustomEnchants_WhiteScroll_Purchased.msg(player);
        Message.MONEY_TAKEN.msg(player, wScrollCost);
    }

    private void attemptPurchaseBScroll(Player player) {
        double balance = VaultHelper.getEconomy().getBalance(player);

        if(balance < bScrollCost) {
            Message.INSUFFICIENT_FUNDS.msg(player, NumberUtils.formatDouble(bScrollCost - balance));
            return;
        }

        ItemStack itemStack = BlackScrollManager.getScroll(1);

        if(player.getInventory().firstEmpty() == -1) {
            player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
        } else {
            player.getInventory().addItem(itemStack);
        }

        VaultHelper.getEconomy().withdrawPlayer(player, bScrollCost);
        Message.CustomEnchants_BlackScroll_Purchased.msg(player);
        Message.MONEY_TAKEN.msg(player, bScrollCost);
    }

    private void attemptPurchaseOScroll(Player player) {
        Message.CustomEnchants_OrganisationScrollsDisabled.msg(player);
        return;
    }

}
