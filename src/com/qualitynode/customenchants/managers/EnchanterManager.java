package com.qualitynode.customenchants.managers;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.handlers.RedeemBookHandler;
import com.qualitynode.customenchants.utils.Rarity;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.ExpUtils;
import net.aminecraftdev.utils.RandomUtils;
import net.aminecraftdev.utils.factory.NbtFactory;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import net.aminecraftdev.utils.message.RomanNumber;
import net.aminecraftdev.utils.voucher.Voucher;
import net.aminecraftdev.utils.voucher.base.VoucherClickAction;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 02-Aug-17
 */
public class EnchanterManager {

    private static final String NBT_UNOPENED_BOOK = "UBook";
    private static final String NBT_OPENED_TYPE_BOOK = "Enchant.Type";
    private static final String NBT_OPENED_LEVEL_BOOK = "Enchant.Level";
    private static final String NBT_SUCCESS = "Rates.Success";
    private static final String NBT_DESTROY = "Rates.Destroy";

    private static Voucher commonVoucher, uncommonVoucher, epicVoucher, legendaryVoucher, mysticalVoucher;
    private static ConfigurationSection unopenedBook, openedBook, enchanterBook;
    private static String itemColor;

    public EnchanterManager(CustomEnchants plugin) {
        unopenedBook = plugin.getConfig().getConfigurationSection("CustomEnchants.Items.UnopenedEnchantBook");
        openedBook = plugin.getConfig().getConfigurationSection("CustomEnchants.Items.OpenedEnchantBook");
        enchanterBook = plugin.getConfig().getConfigurationSection("CustomEnchants.Items.EnchanterBook");
        itemColor = plugin.getConfig().getString("CustomEnchants.Colours.description", "&e");

        loadEnchantBooks();
    }

    private void loadEnchantBooks() {
        VoucherClickAction voucherClickAction = new RedeemBookHandler();
        commonVoucher = new Voucher(getVoucherBook(Rarity.Common), Arrays.asList(NBT_UNOPENED_BOOK))
                .setCheckIfSimilar(false)
                .setTakeItemWhenDone(true)
                .setOnRedeem(voucherClickAction);

        uncommonVoucher = new Voucher(getVoucherBook(Rarity.Uncommon), Arrays.asList(NBT_UNOPENED_BOOK))
                .setCheckIfSimilar(false)
                .setTakeItemWhenDone(true)
                .setOnRedeem(voucherClickAction);

        epicVoucher = new Voucher(getVoucherBook(Rarity.Epic), Arrays.asList(NBT_UNOPENED_BOOK))
                .setCheckIfSimilar(false)
                .setTakeItemWhenDone(true)
                .setOnRedeem(voucherClickAction);

        legendaryVoucher = new Voucher(getVoucherBook(Rarity.Legendary), Arrays.asList(NBT_UNOPENED_BOOK))
                .setCheckIfSimilar(false)
                .setTakeItemWhenDone(true)
                .setOnRedeem(voucherClickAction);

        mysticalVoucher = new Voucher(getVoucherBook(Rarity.Mystical), Arrays.asList(NBT_UNOPENED_BOOK))
                .setCheckIfSimilar(false)
                .setTakeItemWhenDone(true)
                .setOnRedeem(voucherClickAction);
    }

    public static CEnchantment getRandomEnchant(Rarity rarity) {
        List<CEnchantment> allEnchants = EnchantManager.getRarityEnchants().get(rarity);
        CEnchantment customEnchantment;

        if(allEnchants == null) return null;

        do{
            int randomNumber = RandomUtils.getRandomInt(allEnchants.size());

            customEnchantment = allEnchants.get(randomNumber);
        } while(customEnchantment == null);

        return customEnchantment;
    }

    public static int getRandomLevel(CEnchantment enchantment) {
        return RandomUtils.getRandomInt(enchantment.getMaxLevel()) + 1;
    }

    public static void runEnchantCheck(Player player, Rarity rarity) {
        if(!hasEnoughExp(player, rarity)) return;

        int exp = ExpUtils.getTotalExperience(player);
        int cost = rarity.getCost();

        if(player.getInventory().firstEmpty() == -1) {
            Message.INVENTORY_SPACE.msg(player);
            return;
        }

        ExpUtils.setTotalExperience(player, exp - cost);
        player.getInventory().addItem(getUnopenedBook(rarity));
    }

    public static ItemStack getEnchanterBook(Rarity rarity) {
        Map<String, String> replaceMap = new HashMap<>();

        replaceMap.put("{tierColor}", rarity.getPrefix());
        replaceMap.put("{tier}", rarity.name());
        replaceMap.put("{cost}", rarity.getDisplay());

        return ItemStackUtils.createItemStack(enchanterBook, 1, replaceMap);
    }

    public static ItemStack getVoucherBook(Rarity rarity) {
        Map<String, String> replaceMap = new HashMap<>();

        replaceMap.put("{tierColor}", rarity.getPrefix());
        replaceMap.put("{tier}", rarity.name());

        ItemStack itemStack = ItemStackUtils.createItemStack(unopenedBook, 1, replaceMap);
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        compound.put(NBT_UNOPENED_BOOK, rarity.name());

        return craftStack;
    }

    public static ItemStack getUnopenedBook(Rarity rarity) {
        Voucher voucher = getVoucher(rarity);

        if(voucher == null) return new ItemStack(Material.AIR);

        ItemStack cloneCopy = voucher.getVoucher().clone();

        cloneCopy.setAmount(1);

        return cloneCopy;
    }

    public static ItemStack getOpenedBook(CEnchantment enchantment, int level) {
        int success = RandomUtils.getRandomInt(100) + 1;
        int destroy = RandomUtils.getRandomInt(100) + 1;

        return getOpenedBook(enchantment, level, success, destroy);
    }

    public static ItemStack getOpenedBook(CEnchantment enchantment, int level, int success, int destroy) {
        Map<String, String> replaceMap = new HashMap<>();

        replaceMap.put("{tierColor}", enchantment.getRarity().getPrefix());
        replaceMap.put("{tier}", enchantment.getRarity().name());
        replaceMap.put("{enchantLevel}", RomanNumber.toRoman(level));
        replaceMap.put("{enchantName}", enchantment.getName());
        replaceMap.put("{s}", ""+success);
        replaceMap.put("{d}", ""+destroy);
        replaceMap.put("{description}", enchantment.getDescription().replace("{c}", itemColor));
        replaceMap.put("{type}", enchantment.getEnchantmentType().toString());

        ItemStack itemStack = ItemStackUtils.createItemStack(openedBook, 1, replaceMap);
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        compound.put(NBT_OPENED_TYPE_BOOK, enchantment.getName());
        compound.put(NBT_OPENED_LEVEL_BOOK, level);
        compound.put(NBT_DESTROY, destroy);
        compound.put(NBT_SUCCESS, success);

        return craftStack;
    }

    public static Rarity getRarity(ItemStack itemStack) {
        if(!isUnopenedBook(itemStack)) return null;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return Rarity.valueOf(compound.getString(NBT_UNOPENED_BOOK, null));
    }

    public static CEnchantment getEnchantment(ItemStack itemStack) {
        if(!hasEnchantmentTag(itemStack)) return null;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return EnchantManager.getEnchants().getOrDefault(compound.getString(NBT_OPENED_TYPE_BOOK, null).toUpperCase(), null);
    }

    public static int getLevel(ItemStack itemStack) {
        if(!hasLevelTag(itemStack)) return -1;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.getInteger(NBT_OPENED_LEVEL_BOOK, -1);
    }

    public static int getSucessRate(ItemStack itemStack) {
        if(!hasSuccessRate(itemStack)) return -1;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.getInteger(NBT_SUCCESS, -1);
    }

    public static ItemStack increaseSuccessRate(ItemStack itemStack, int increaseAmount) {
        if(!hasSuccessRate(itemStack)) return itemStack;

        int success = getSucessRate(itemStack);
        int destroy = getDestroyRate(itemStack);
        CEnchantment enchantment = getEnchantment(itemStack);
        int level = getLevel(itemStack);

        return getOpenedBook(enchantment, level, success + increaseAmount, destroy);
    }

    public static int getDestroyRate(ItemStack itemStack) {
        if(!hasDestroyRate(itemStack)) return -1;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.getInteger(NBT_DESTROY, -1);
    }

    public static boolean isApplicationSuccessful(int chance) {
        int random = RandomUtils.getRandomInt(100) + 1;

        return random <= chance;
    }

    public static boolean hasEnchantmentTag(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        if(!compound.containsKey(NBT_OPENED_TYPE_BOOK)) return false;
        return compound.getString(NBT_OPENED_TYPE_BOOK, null) != null;
    }

    public static boolean hasDestroyRate(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        if(!compound.containsKey(NBT_DESTROY)) return false;
        return compound.getInteger(NBT_DESTROY, -1) != -1;
    }

    public static boolean hasSuccessRate(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        if(!compound.containsKey(NBT_SUCCESS)) return false;
        return compound.getInteger(NBT_SUCCESS, -1) != -1;
    }

    public static boolean hasLevelTag(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        if(!compound.containsKey(NBT_OPENED_LEVEL_BOOK)) return false;
        return compound.getInteger(NBT_OPENED_LEVEL_BOOK, -1) != -1;
    }

    private static Voucher getVoucher(Rarity rarity) {
        switch (rarity) {
            case Epic:
                return epicVoucher;
            case Common:
                return commonVoucher;
            case Mystical:
                return mysticalVoucher;
            case Uncommon:
                return uncommonVoucher;
            case Legendary:
                return legendaryVoucher;
        }

        return null;
    }

    private static boolean isUnopenedBook(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        if(!compound.containsKey(NBT_UNOPENED_BOOK)) return false;
        return compound.getString(NBT_UNOPENED_BOOK, null) != null;
    }

    private static boolean hasEnoughExp(Player player, Rarity rarity) {
        int required = rarity.getCost();
        int toal = ExpUtils.getTotalExperience(player);

        if(toal < required) {
            Message.CustomEnchants_ExpNotEnough.msg(player);
            return false;
        }

        return true;
    }

}
