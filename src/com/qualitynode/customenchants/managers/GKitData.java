package com.qualitynode.customenchants.managers;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.handlers.GKit;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.core.PlayerData;
import net.aminecraftdev.utils.inventory.Panel;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 21-Oct-17
 */
public class GKitData {

    private static final List<GKit> LIST_OF_GKITS = new ArrayList<>();
    private static CustomEnchants PLUGIN;

    public GKitData() {}

    public GKitData(CustomEnchants plugin) {
        PLUGIN = plugin;
    }

    public void loadGKits() {
        LIST_OF_GKITS.clear();

        ConfigurationSection section = PLUGIN.getGKits().getConfigurationSection("");

        for(String gkitName : section.getKeys(false)) {
            ConfigurationSection configurationSection = section.getConfigurationSection(gkitName);

            GKit gKit = new GKit(configurationSection);
            PlayerData.addDefaultObject("GKit." + gkitName, 0);

            LIST_OF_GKITS.add(gKit);
        }
    }

    public void addToPanel(Panel panel, Player player) {
        for(GKit gKit : LIST_OF_GKITS) {
            panel.setItem(gKit.getSlot(), gKit.getItemStack(player), (event) -> {
                if(!gKit.hasPermission(player)) {
                    Message.GKit_NoAccess.msg(player);
                    return;
                }

                if(!gKit.canPlayerUseKit(player)) {
                    gKit.sendCooldownMessage(player);
                    return;
                }

                gKit.givePlayerKit(player);
                gKit.applyCooldown(player);
                Message.GKit_Claimed.msg(player);
                panel.setItem(gKit.getSlot(), gKit.getItemStack(player));

                return;
            });
        }
    }
}
