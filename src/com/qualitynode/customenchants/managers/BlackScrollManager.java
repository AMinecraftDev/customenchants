package com.qualitynode.customenchants.managers;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.RandomUtils;
import net.aminecraftdev.utils.factory.NbtFactory;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 02-Aug-17
 */
public class BlackScrollManager {

    private static final String NBT_BLACKSCROLL = "BlackScrollItem";

    private static ConfigurationSection blackScroll;

    public BlackScrollManager(CustomEnchants plugin) {
        blackScroll = plugin.getConfig().getConfigurationSection("CustomEnchants.Items.BlackScroll");
    }

    public static ItemStack getScroll(int amount) {
        ItemStack itemStack = ItemStackUtils.createItemStack(blackScroll, amount, null);
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        compound.put(NBT_BLACKSCROLL, UUID.randomUUID().toString());
        return craftStack;
    }

    public static boolean isBlackScroll(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.containsKey(NBT_BLACKSCROLL);
    }

    public static ItemStack removeRandomEnchant(Player player, ItemStack itemStack) {
        Map<CEnchantment, Integer> activeEnchants = new HashMap<>();
        Map<CEnchantment, String> enchantKeys = new HashMap<>();
        List<CEnchantment> enchants = new ArrayList<>();
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        Set<Map.Entry<String, Object>> entrySet = compound.entrySet();

        for(Map.Entry<String, Object> entry : entrySet) {
            String key = entry.getKey();

            if(key.startsWith(EnchantManager.getNBTEnchantPath())) {
                int level = (int) entry.getValue();
                CEnchantment enchantment = EnchantManager.getEnchants().get(key.replace(EnchantManager.getNBTEnchantPath(), "").toUpperCase());

                if(enchantment == null) continue;
                if(enchants.contains(enchantment)) continue;

                activeEnchants.put(enchantment, level);
                enchants.add(enchantment);
                enchantKeys.put(enchantment, key);
            }
        }

        if(activeEnchants.isEmpty()) return itemStack;

        int amount = EnchantManager.getEnchantsAmount(itemStack);
        int random = RandomUtils.getRandomInt(activeEnchants.size());
        CEnchantment enchantment = enchants.get(random);
        int level = activeEnchants.get(enchantment);
        String key = enchantKeys.get(enchantment);
        ItemStack book = EnchanterManager.getOpenedBook(enchantment, level);

        compound.remove(key);
        compound.put(EnchantManager.getNbtEnchAmount(), amount-1);
        craftStack.setItemMeta(EnchantManager.removeEnchantMeta(craftStack.getItemMeta(), enchantment, level));

        if(player.getInventory().firstEmpty() == -1) {
            player.getWorld().dropItemNaturally(player.getLocation(), book);
            Message.INVENTORY_SPACE.msg(player);
        } else {
            player.getInventory().addItem(book);
        }
        return craftStack;
    }

}
