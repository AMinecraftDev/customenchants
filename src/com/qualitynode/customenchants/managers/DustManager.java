package com.qualitynode.customenchants.managers;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.utils.Rarity;
import net.aminecraftdev.utils.factory.NbtFactory;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class DustManager {

    private static final String NBT_DUST_AMOUNT = "EnchantDust.Amount";
    private static final String NBT_DUST_RARITY = "EnchantDust.Rarity";

    private static ConfigurationSection dust;

    public DustManager(CustomEnchants plugin) {
        dust = plugin.getConfig().getConfigurationSection("CustomEnchants.Items.Dust");
    }

    public static ItemStack getDust(Rarity rarity, int success, int amount) {
        Map<String, String> replaceMap = new HashMap<>();

        replaceMap.put("{tierColor}", rarity.getPrefix());
        replaceMap.put("{tier}", rarity.name());
        replaceMap.put("{increase}", ""+success);

        ItemStack itemStack = ItemStackUtils.createItemStack(dust, amount, replaceMap);
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        compound.put(NBT_DUST_AMOUNT, success);
        compound.put(NBT_DUST_RARITY, rarity.name());

        return craftStack;
    }

    public static boolean isDust(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.containsKey(NBT_DUST_RARITY);
    }

    public static int getDustAmount(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.getInteger(NBT_DUST_AMOUNT, 0);
    }

    public static Rarity getDustRarity(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        if(!compound.containsKey(NBT_DUST_RARITY)) return null;
        return Rarity.valueOf(compound.getString(NBT_DUST_RARITY, null));
    }

}
