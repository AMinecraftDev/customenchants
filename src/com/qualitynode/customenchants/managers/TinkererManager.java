package com.qualitynode.customenchants.managers;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.utils.Rarity;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.inventory.Panel;
import net.aminecraftdev.utils.inventory.PanelBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 02-Aug-17
 */
public class TinkererManager {

    private static final int[] slots = {0,1,2,3, 9,10,11,12, 18,19,20,21, 27,28,29,30, 36,37,38,39, 45,46,47,48};
    private static final int[] opposite = {4,5,6,7,8, 13,14,15,16,17, 22,23,24,25,26, 31,32,33,34,35, 40,41,42,43,44, 49,50,51,52,53};
    private static final Map<UUID, Map<Integer, ItemStack>> ITEMS_MAP = new HashMap<>();
    private static final Map<UUID, Map<Integer, Integer>> AMOUNT_MAP = new HashMap<>();

    private static ConfigurationSection tinkererGUI;
    private static List<Integer> acceptSlots, dividerSlots;

    public TinkererManager(CustomEnchants plugin) {
        tinkererGUI = plugin.getInventories().getConfigurationSection("CustomEnchants.TinkererGUI");
        acceptSlots = new ArrayList<>();
        dividerSlots = new ArrayList<>();

        for(String s : tinkererGUI.getConfigurationSection("Items").getKeys(false)) {
            ConfigurationSection innerSection = tinkererGUI.getConfigurationSection("Items." + s);
            int slot = Integer.valueOf(s) - 1;

            if(innerSection.contains("Accept") && innerSection.getBoolean("Accept")) acceptSlots.add(slot);
            if(innerSection.contains("Divider") && innerSection.getBoolean("Divider")) dividerSlots.add(slot);
        }
    }

    public static void handleGUI(Player p) {
        Panel panel = new PanelBuilder(tinkererGUI).getPanel()
                .setDestroyWhenDone(true)
                .setCancelClick(true);

        panel.setOnClose(player -> {
            Inventory inventory = player.getOpenInventory().getTopInventory();

            for(int i = 0; i < inventory.getSize(); i++) {
                boolean contains = false;

                for(int j : slots) {
                    if(i == j) {
                        contains = true;
                        break;
                    }
                }

                if(!contains) continue;

                removeItem(player.getUniqueId(), i, inventory);
            }
        });

        for(int i : acceptSlots) {
            panel.setOnClick(i, event -> finishTrade((Player) event.getWhoClicked()));
        }

        panel.setOnClick(event -> {
            int rawSlot = event.getRawSlot();
            int slot = event.getSlot();
            Player player = (Player) event.getWhoClicked();
            Inventory inventory = player.getOpenInventory().getTopInventory();

            if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR) return;

            ItemStack itemStack = event.getCurrentItem();

            if(event.getClick() == ClickType.SHIFT_LEFT) return;
            if(!EnchanterManager.hasEnchantmentTag(itemStack)) return;

            if(rawSlot > (inventory.getSize() - 1)) {
                ItemStack traded = itemStack.clone();

                if(event.getClick() == ClickType.RIGHT || event.getClick() == ClickType.SHIFT_RIGHT) {
                    if(itemStack.getAmount() != 1) {
                        int half = itemStack.getAmount() / 2;

                        traded.setAmount(half);
                    }
                }

                if(!addItem(player.getUniqueId(), traded, player.getOpenInventory().getTopInventory())) return;

                player.getInventory().removeItem(traded);
                player.updateInventory();
            } else {
                for(int i : opposite) {
                    if(i != slot) continue;

                    Message.CustomEnchants_Tinkerer_OppositeSide.msg(player);
                    return;
                }

                if(acceptSlots.contains(slot)) return;
                if(dividerSlots.contains(slot)) return;

                removeItem(player.getUniqueId(), slot, inventory);
            }
        });

        panel.openFor(p);
    }

    private static void finishTrade(Player player) {
        UUID uuid = player.getUniqueId();

        if(!ITEMS_MAP.containsKey(uuid)) return;
        if(!AMOUNT_MAP.containsKey(uuid)) return;

        Map<Integer, ItemStack> personalItems = ITEMS_MAP.get(uuid);
        Map<Integer, Integer> personalDusts = AMOUNT_MAP.get(uuid);

        for(int i : personalItems.keySet()) {
            int amount = personalDusts.get(i);
            CEnchantment enchantment = EnchanterManager.getEnchantment(personalItems.get(i));

            if(enchantment == null) continue;

            Rarity rarity = enchantment.getRarity();

            if(rarity == null) continue;

            ItemStack dust = DustManager.getDust(rarity, amount, 1);

            player.getInventory().addItem(dust);
        }

        ITEMS_MAP.remove(uuid);
        AMOUNT_MAP.remove(uuid);
        player.closeInventory();
        Message.CustomEnchants_Tinkerer_Successful.msg(player);
    }

    private static void removeItem(UUID uuid, int slot, Inventory inventory) {
        if(!ITEMS_MAP.containsKey(uuid)) return;

        Map<Integer, ItemStack> personalItems = ITEMS_MAP.get(uuid);
        Map<Integer, Integer> personalDusts = AMOUNT_MAP.get(uuid);

        if(!personalDusts.containsKey(slot)) return;
        if(!personalItems.containsKey(slot)) return;

        ItemStack itemStack = personalItems.get(slot);

        inventory.setItem(slot, null);

        if(slot <= 8) {
            inventory.setItem(slot + 4, null);
        } else {
            inventory.setItem(slot + 5, null);
        }

        Bukkit.getPlayer(uuid).getInventory().addItem(itemStack);
        personalDusts.remove(slot);
        personalItems.remove(slot);
        ITEMS_MAP.put(uuid, personalItems);
        AMOUNT_MAP.put(uuid, personalDusts);
    }

    private static boolean addItem(UUID uuid, ItemStack itemStack, Inventory inventory) {
        int slot = -1;

        for(int i : slots) {
            if(i > (inventory.getSize() - 1)) return false;

            if(inventory.getItem(i) == null || inventory.getItem(i).getType() == Material.AIR) {
                inventory.setItem(i, itemStack);
                slot = i;
                break;
            }
        }

        if(slot == -1) return false;

        Map<Integer, ItemStack> personalItems = ITEMS_MAP.getOrDefault(uuid, new HashMap<>());
        Map<Integer, Integer> personalDusts = AMOUNT_MAP.getOrDefault(uuid, new HashMap<>());
        int amount = getValue(itemStack);
        CEnchantment enchantment = EnchanterManager.getEnchantment(itemStack);

        if(enchantment == null) return false;

        Rarity rarity = enchantment.getRarity();

        if(rarity == null) return false;

        ItemStack dust = DustManager.getDust(rarity, amount, 1);

        personalDusts.put(slot, amount);
        personalItems.put(slot, itemStack);

        if(slot <= 8) {
            inventory.setItem(slot + 4, dust);
        } else {
            inventory.setItem(slot + 5, dust);
        }

        ITEMS_MAP.put(uuid, personalItems);
        AMOUNT_MAP.put(uuid, personalDusts);
        return true;
    }

    private static int getValue(ItemStack itemStack) {
        int successRate = EnchanterManager.getSucessRate(itemStack);
        double value = (double) successRate / 10;

        return Math.round(value) <= 0? 1 : (int) Math.round(value);
    }
}
