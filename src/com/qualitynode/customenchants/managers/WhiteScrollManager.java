package com.qualitynode.customenchants.managers;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.factory.NbtFactory;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 02-Aug-17
 */
public class WhiteScrollManager {

    private static final String NBT_WHITESCROLL = "WhiteScrollItem";
    private static final String NBT_WHITESCROLLED = "WhiteScroll";

    private static ConfigurationSection whitescroll;

    public WhiteScrollManager(CustomEnchants plugin) {
        whitescroll = plugin.getConfig().getConfigurationSection("CustomEnchants.Items.WhiteScroll");
    }

    public static ItemStack getScroll(int amount) {
        ItemStack itemStack = ItemStackUtils.createItemStack(whitescroll, amount, null);
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        compound.put(NBT_WHITESCROLL, UUID.randomUUID().toString());
        return craftStack;
    }

    public static boolean isWhiteScroll(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.containsKey(NBT_WHITESCROLL);
    }

    public static ItemStack addWhiteScrollEffect(ItemStack itemStack) {
        if(hasWhitescroll(itemStack)) return itemStack;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        compound.put(NBT_WHITESCROLLED, UUID.randomUUID().toString());

        ItemMeta itemMeta = craftStack.getItemMeta();
        List<String> lore = itemMeta.hasLore()? itemMeta.getLore() : new ArrayList<>();

        lore.add(Message.CustomEnchants_WhiteScrollLoreFormat.toString());

        itemMeta.setLore(lore);
        craftStack.setItemMeta(itemMeta);

        return craftStack;
    }

    public static ItemStack takeWhiteScrollEffect(ItemStack itemStack) {
        if(!hasWhitescroll(itemStack)) return itemStack;

        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        compound.remove(NBT_WHITESCROLLED);

        ItemMeta itemMeta = craftStack.getItemMeta();
        List<String> lore = itemMeta.hasLore()? itemMeta.getLore() : new ArrayList<>();
        List<String> newLore = new ArrayList<>();

        lore.forEach(s -> {
            if(!s.equals(Message.CustomEnchants_WhiteScrollLoreFormat.toString())) {
                newLore.add(s);
            }
        });

        itemMeta.setLore(newLore);
        craftStack.setItemMeta(itemMeta);

        return craftStack;
    }

    public static boolean hasWhitescroll(ItemStack itemStack) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(itemStack);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        return compound.containsKey(NBT_WHITESCROLLED);
    }


}
