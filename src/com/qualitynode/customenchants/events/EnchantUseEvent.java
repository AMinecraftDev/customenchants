package com.qualitynode.customenchants.events;

import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.utils.EnchantmentType;
import com.qualitynode.customenchants.utils.Rarity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 10-Aug-17
 */
public class EnchantUseEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private Player player;
    private CEnchantment enchantment;
    private int level;
    private boolean cancelled = false;

    public EnchantUseEvent(Player player, CEnchantment enchantment, int level) {
        this.player = player;
        this.enchantment = enchantment;
        this.level = level;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public CEnchantment getEnchantment() {
        return enchantment;
    }

    public Rarity getRarity() {
        return getEnchantment().getRarity();
    }

    public EnchantmentType getEnchantmentType() {
        return getEnchantment().getEnchantmentType();
    }

    public int getLevel() {
        return level;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
