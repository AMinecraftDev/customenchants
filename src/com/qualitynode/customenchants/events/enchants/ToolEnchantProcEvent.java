package com.qualitynode.customenchants.events.enchants;

import com.qualitynode.customenchants.events.EnchantProcEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 */
public class ToolEnchantProcEvent extends EnchantProcEvent {

    private Map<Block, ItemStack> drops = new HashMap<>();
    private List<Block> blocks;
    private int expToDrop;

    public ToolEnchantProcEvent(Player player, List<Block> blocks, int expToDrop) {
        super(player);

        this.blocks = blocks;
        this.expToDrop = expToDrop;
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public Map<Block, ItemStack> getDrops() {
        return drops;
    }

    public void addBlock(Block block) {
        if(blocks.contains(block)) return;

        blocks.add(block);
    }

    public void setDrops(Block block, ItemStack itemStack) {
        if(!this.blocks.contains(block)) return;

        drops.put(block, itemStack);
    }

    public int getExpToDrop() {
        return expToDrop;
    }

    public void setExpToDrop(int expToDrop) {
        this.expToDrop = expToDrop;
    }
}
