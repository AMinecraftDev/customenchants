package com.qualitynode.customenchants.events.enchants;

import com.qualitynode.customenchants.events.EnchantProcEvent;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 */
public class BowEnchantProcEvent extends EnchantProcEvent {

    private double damage, originalDamage;
    private LivingEntity target;

    public BowEnchantProcEvent(Player player, LivingEntity target, double damage) {
        super(player);

        this.target = target;
        this.damage = damage;
        this.originalDamage = damage;
    }

    public LivingEntity getTarget() {
        return target;
    }

    public double getOriginalDamage() {
        return originalDamage;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }
}
