package com.qualitynode.customenchants.events.enchants;

import com.qualitynode.customenchants.events.EnchantProcEvent;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 26-Jun-17
 */
public class ArmorEnchantProcEvent extends EnchantProcEvent {

    private LivingEntity target;
    private double damage;

    public ArmorEnchantProcEvent(Player player, LivingEntity target, double damage) {
        super(player);

        this.target = target;
        this.damage = damage;
    }

    public LivingEntity getTarget() {
        return target;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double newDamage) {
        this.damage = newDamage;
    }
}
