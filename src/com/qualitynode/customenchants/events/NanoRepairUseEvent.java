package com.qualitynode.customenchants.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 14-Aug-17
 */
public class NanoRepairUseEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private Player player;
    private int level;
    private ItemStack itemStack;
    private boolean cancelled = false;

    public NanoRepairUseEvent(Player player, ItemStack itemStack, int level) {
        this.player = player;
        this.level = level;
        this.itemStack = itemStack;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public int getLevel() {
        return level;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
