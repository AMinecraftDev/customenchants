package com.qualitynode.customenchants.enchants;

import com.qualitynode.customenchants.events.ArmorEquipEvent;
import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.managers.EnchantManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 14-Aug-17
 */
public class PotionEnchants implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onEquip(ArmorEquipEvent event) {
        Player player = event.getPlayer();
        ItemStack newItem = event.getNewArmorPiece();
        ItemStack oldItem = event.getOldArmorPiece();

        /**
         * Removing the potion effects
         */
        if(EnchantManager.hasEnchants(oldItem)) {
            for(CEnchantment enchantment : EnchantManager.getEnchantmentPotions().keySet()) {
                if(!EnchantManager.hasEnchant(oldItem, enchantment)) continue;
                if(!enchantment.isEnabled()) continue;

                Map<PotionEffectType, Integer> effects = EnchantManager.getUpdatedEffects(player, null, oldItem, enchantment);

                for(PotionEffectType type : effects.keySet()) {
                    if(effects.get(type) < 0) {
                        player.removePotionEffect(type);
                    } else {
                        player.removePotionEffect(type);
                        player.addPotionEffect(new PotionEffect(type, Integer.MAX_VALUE, effects.get(type)));
                    }
                }
            }
        }

        /**
         * Adding the potion effects
         */
        if(EnchantManager.hasEnchants(newItem)) {

            for(CEnchantment enchantment : EnchantManager.getEnchantmentPotions().keySet()) {
                if(!EnchantManager.hasEnchant(newItem, enchantment)) continue;
                if(!enchantment.isEnabled()) continue;

                Map<PotionEffectType, Integer> effects = EnchantManager.getUpdatedEffects(player, newItem, oldItem, enchantment);

                for(PotionEffectType type : effects.keySet()) {
                    if(effects.get(type) < 0) {
                        player.removePotionEffect(type);
                    } else {
                        player.removePotionEffect(type);
                        player.addPotionEffect(new PotionEffect(type, Integer.MAX_VALUE, effects.get(type)));
                    }
                }

                break;
            }
        }
    }

}
