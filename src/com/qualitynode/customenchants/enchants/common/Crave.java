package com.qualitynode.customenchants.enchants.common;

import com.qualitynode.customenchants.handlers.types.armour.HelmetEnchantment;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Crave extends HelmetEnchantment {

    @Override
    public String getName() {
        return "Crave";
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Location from = event.getFrom();
        Location to = event.getTo();

        if(from.getBlockX() == to.getBlockX() && from.getBlockY() == to.getBlockY() && from.getBlockZ() == to.getBlockZ()) return;

        Player player = event.getPlayer();
        int level = getEnchantLevel(player, this);

        if(player.getFoodLevel() > 20) return;
        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        if(player.getFoodLevel()+1 <= 20) {
            player.setFoodLevel(player.getFoodLevel()+1);
        } else {
            player.setFoodLevel(20);
        }
    }
}
