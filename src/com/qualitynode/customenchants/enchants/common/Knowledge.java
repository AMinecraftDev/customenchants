package com.qualitynode.customenchants.enchants.common;

import com.qualitynode.customenchants.handlers.types.weapons.SwordEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Knowledge extends SwordEnchantment {

    @Override
    public String getName() {
        return "Knowledge";
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onGrind(EntityDeathEvent event) {
        LivingEntity livingEntity = event.getEntity();
        EntityDamageEvent entityDamageEvent = livingEntity.getLastDamageCause();

        if(!(entityDamageEvent instanceof EntityDamageByEntityEvent)) return;

        EntityDamageByEntityEvent entityDamageByEntityEvent = (EntityDamageByEntityEvent) entityDamageEvent;

        if(!(entityDamageByEntityEvent.getEntity() instanceof LivingEntity)) return;
        if(!(entityDamageByEntityEvent.getDamager() instanceof Player)) return;

        Player damager = (Player) entityDamageByEntityEvent.getDamager();
        int level = getEnchantLevel(damager, this);

        if(!hasEnchant(damager, this)) return;
        if(!canExecute(damager, this, level)) return;

        event.setDroppedExp(event.getDroppedExp() * (level));
    }
}
