package com.qualitynode.customenchants.enchants.common;

import com.qualitynode.customenchants.handlers.types.ArmourEnchantment;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class BurnShield extends ArmourEnchantment {

    @Override
    public String getName() {
        return "BurnShield";
    }

}
