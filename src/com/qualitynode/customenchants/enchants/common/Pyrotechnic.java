package com.qualitynode.customenchants.enchants.common;

import com.qualitynode.customenchants.events.enchants.ArmorEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.ArmourEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Pyrotechnic extends ArmourEnchantment {

    @Override
    public String getName() {
        return "Pyrotechnic";
    }

    @EventHandler
    public void onArmour(ArmorEnchantProcEvent event) {
        Player player = event.getPlayer();
        LivingEntity livingEntity = event.getTarget();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        livingEntity.setFireTicks(100);
    }
}
