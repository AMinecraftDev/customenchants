package com.qualitynode.customenchants.enchants.common;

import com.qualitynode.customenchants.handlers.types.armour.HelmetEnchantment;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Torch extends HelmetEnchantment {

    @Override
    public String getName() {
        return "Torch";
    }

}
