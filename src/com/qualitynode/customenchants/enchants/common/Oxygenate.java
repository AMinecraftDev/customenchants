package com.qualitynode.customenchants.enchants.common;

import com.qualitynode.customenchants.handlers.types.ToolEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Collection;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Oxygenate extends ToolEnchantment {

    @Override
    public String getName() {
        return "Oxygenate";
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onClick(InventoryClickEvent event) {
        if(event.isCancelled()) return;
        if(!(event.getWhoClicked() instanceof Player)) return;

        Player player = (Player) event.getWhoClicked();
        int level = getEnchantLevel(player, this);
        int slot = event.getSlot();
        int heldItemSlot = player.getInventory().getHeldItemSlot();
        PotionEffectType potionEffectType = PotionEffectType.WATER_BREATHING;
        PotionEffect customPotionEffect = new PotionEffect(potionEffectType, 100000, level);

        if(slot != heldItemSlot) return;

        if(hasEnchant(event.getCurrentItem(), this)) {
            player.removePotionEffect(potionEffectType);
        }

        if(hasEnchant(event.getCursor(), this)) {
            player.removePotionEffect(potionEffectType);
            player.addPotionEffect(customPotionEffect);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onItemHeld(PlayerItemHeldEvent event) {
        if(event.isCancelled()) return;

        Player player = event.getPlayer();
        int level = getEnchantLevel(player, this);
        PotionEffectType potionEffectType = PotionEffectType.WATER_BREATHING;
        PotionEffect customPotionEffect = new PotionEffect(potionEffectType, Integer.MAX_VALUE, level - 1);
        ItemStack itemStack = event.getPlayer().getInventory().getItem(event.getNewSlot());

        if(!canEnchantItem(itemStack, this)) return;
        if(!hasEnchant(event.getPlayer().getInventory().getItem(event.getNewSlot()), this)) {
            Collection<PotionEffect> collection = player.getActivePotionEffects();

            for(PotionEffect potionEffect : collection) {
                if(!potionEffect.getType().equals(customPotionEffect.getType())) continue;
                if(potionEffect.getDuration() < 10000) continue;

                player.removePotionEffect(potionEffectType);
                break;
            }

            return;
        }

        player.removePotionEffect(potionEffectType);
        player.addPotionEffect(customPotionEffect);
    }
}
