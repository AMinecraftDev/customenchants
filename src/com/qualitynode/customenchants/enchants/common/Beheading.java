package com.qualitynode.customenchants.enchants.common;

import com.qualitynode.customenchants.handlers.types.weapons.SwordEnchantment;
import net.aminecraftdev.utils.PluginUtils;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Beheading extends SwordEnchantment {

    private ConfigurationSection itemStackSection;

    public Beheading() {
        this.itemStackSection = PLUGIN.getConfig().getConfigurationSection("CustomEnchants.Items.BeheadingItem");
    }

    @Override
    public String getName() {
        return "Beheading";
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        if(!(event.getEntity().getKiller() instanceof Player)) return;

        Player player = event.getEntity();
        Player killer = player.getKiller();
        ItemStack itemStack = PluginUtils.getItemInHand(killer);
        int level = getEnchantLevel(itemStack, this);

        if(!hasEnchant(itemStack, this)) return;
        if(!canExecute(player, this, level)) return;

        String name = player.getName();
        Map<String, String> map = new HashMap<>();

        map.put("{n}", name);

        ItemStack headToDrop = ItemStackUtils.createItemStack(this.itemStackSection, 1, map);

        event.getDrops().add(headToDrop);
    }
}
