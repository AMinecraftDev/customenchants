package com.qualitynode.customenchants.enchants.common;

import com.qualitynode.customenchants.events.enchants.WeaponEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.weapons.AxeEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class FeedMe extends AxeEnchantment {

    @Override
    public String getName() {
        return "FeedMe";
    }

    @EventHandler
    public void onWeaponUse(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        LivingEntity target = event.getTarget();
        ItemStack itemStack = player.getItemInHand();
        int level = getEnchantLevel(itemStack, this);

        if(player.getFoodLevel() > 20) return;
        if(!hasEnchant(itemStack, this)) return;
        if(!canExecute(player, this, level)) return;

        if(player.getFoodLevel()+1 <= 20) {
            player.setFoodLevel(player.getFoodLevel()+1);
        } else {
            player.setFoodLevel(20);
        }
    }
}
