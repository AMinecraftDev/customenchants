package com.qualitynode.customenchants.enchants.mystical;

import com.qualitynode.customenchants.handlers.types.armour.BootsEnchantment;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class MoonWalker extends BootsEnchantment {

    @Override
    public String getName() {
        return "MoonWalker";
    }
}
