package com.qualitynode.customenchants.enchants.mystical;

import com.qualitynode.customenchants.handlers.types.ArmourEnchantment;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Drunk extends ArmourEnchantment {

    @Override
    public String getName() {
        return "Drunk";
    }

}
