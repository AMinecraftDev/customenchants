package com.qualitynode.customenchants.enchants.mystical;

import com.qualitynode.customenchants.events.enchants.WeaponEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.weapons.SwordEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class DoubleTap extends SwordEnchantment {

    @Override
    public String getName() {
        return "DoubleTap";
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onWeapon(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        event.setDamage(event.getOriginalDamage() * 2);
    }
}
