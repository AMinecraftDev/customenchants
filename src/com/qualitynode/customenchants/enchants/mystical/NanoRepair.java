package com.qualitynode.customenchants.enchants.mystical;

import com.qualitynode.customenchants.events.NanoRepairUseEvent;
import com.qualitynode.customenchants.handlers.types.AllEnchantment;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class NanoRepair extends AllEnchantment {

    @Override
    public String getName() {
        return "NanoRepair";
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        Location to = event.getTo();
        Location from = event.getFrom();

        if(from.getBlockX() == to.getBlockX() && from.getBlockY() == to.getBlockY() && from.getBlockZ() == to.getBlockZ()) return;

        for(ItemStack itemStack : player.getInventory().getArmorContents()) {
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;

            int level = getEnchantLevel(itemStack, this);
            int durability = itemStack.getDurability() - level;

            if(!hasEnchant(itemStack, this)) continue;
            if(itemStack.getDurability() < 0) continue;
            if(!canExecuteChance(level)) continue;

            NanoRepairUseEvent nanoRepairUseEvent = new NanoRepairUseEvent(player, itemStack, level);
            Bukkit.getPluginManager().callEvent(nanoRepairUseEvent);

            if(nanoRepairUseEvent.isCancelled()) continue;

            if(durability > 0) {
                itemStack.setDurability((short) durability);
            } else {
                itemStack.setDurability((short) 0);
            }
        }

    }
}
