package com.qualitynode.customenchants.enchants.mystical;

import com.qualitynode.customenchants.events.enchants.BowEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.weapons.BowEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.util.Vector;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Grapple extends BowEnchantment {

    @Override
    public String getName() {
        return "Grapple";
    }

    @EventHandler
    public void onBow(BowEnchantProcEvent event) {
        Player player = event.getPlayer();
        LivingEntity livingEntity = event.getTarget();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        Vector vector = player.getLocation().toVector().subtract(livingEntity.getLocation().toVector()).normalize().multiply(3).setY(2);

        livingEntity.setVelocity(vector);
    }
}
