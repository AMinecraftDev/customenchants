package com.qualitynode.customenchants.enchants.mystical;

import com.qualitynode.customenchants.events.PickPocketUseEvent;
import com.qualitynode.customenchants.events.enchants.WeaponEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.weapons.SwordEnchantment;
import net.aminecraftdev.utils.RandomUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class PickPocket extends SwordEnchantment {

    @Override
    public String getName() {
        return "PickPocket";
    }

    @EventHandler
    public void onWeapon(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        LivingEntity livingEntity = event.getTarget();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;
        if(!(livingEntity instanceof Player)) return;
        if(livingEntity.isDead()) return;

        int slot = RandomUtils.getRandomInt(4) + 1;
        ItemStack targetItemStack;

        switch (slot) {
            case 1:
                targetItemStack = livingEntity.getEquipment().getHelmet();
                break;
            case 2:
                targetItemStack = livingEntity.getEquipment().getChestplate();
                break;
            case 3:
                targetItemStack = livingEntity.getEquipment().getLeggings();
                break;
            case 4:
                targetItemStack = livingEntity.getEquipment().getBoots();
                break;
            default:
                targetItemStack = null;
                break;
        }

        if(targetItemStack == null) return;

        PickPocketUseEvent pickPocketUseEvent = new PickPocketUseEvent(player, livingEntity, targetItemStack, level);
        Bukkit.getPluginManager().callEvent(pickPocketUseEvent);

        if(pickPocketUseEvent.isCancelled()) return;

        switch (slot) {
            case 1:
                livingEntity.getEquipment().setHelmet(null);
                break;
            case 2:
                livingEntity.getEquipment().setChestplate(null);
                break;
            case 3:
                livingEntity.getEquipment().setLeggings(null);
                break;
            case 4:
                livingEntity.getEquipment().setBoots(null);
                break;
        }

        ((Player) livingEntity).getInventory().addItem(targetItemStack);
    }
}
