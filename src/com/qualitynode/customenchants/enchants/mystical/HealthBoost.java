package com.qualitynode.customenchants.enchants.mystical;

import com.qualitynode.customenchants.handlers.types.ArmourEnchantment;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 23-Oct-17
 */
public class HealthBoost extends ArmourEnchantment {

    @Override
    public String getName() {
        return "HealthBoost";
    }
}
