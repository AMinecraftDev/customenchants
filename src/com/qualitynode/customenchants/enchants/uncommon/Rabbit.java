package com.qualitynode.customenchants.enchants.uncommon;

import com.qualitynode.customenchants.handlers.types.armour.BootsEnchantment;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Rabbit extends BootsEnchantment {

    @Override
    public String getName() {
        return "Rabbit";
    }
}
