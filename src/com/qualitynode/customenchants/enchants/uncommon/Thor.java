package com.qualitynode.customenchants.enchants.uncommon;

import com.qualitynode.customenchants.events.enchants.ArmorEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.ArmourEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Thor extends ArmourEnchantment {

    @Override
    public String getName() {
        return "Thor";
    }

    @EventHandler
    public void onArmour(ArmorEnchantProcEvent event) {
        Player player = event.getPlayer();
        LivingEntity livingEntity = event.getTarget();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        livingEntity.getWorld().strikeLightningEffect(livingEntity.getLocation());
        livingEntity.damage(5);
    }
}
