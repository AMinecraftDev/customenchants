package com.qualitynode.customenchants.enchants.uncommon;

import com.qualitynode.customenchants.handlers.types.ArmourEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Recover extends ArmourEnchantment {

    @Override
    public String getName() {
        return "Recover";
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        if(!(event.getEntity().getKiller() instanceof Player)) return;

        Player livingEntity = event.getEntity();
        Player player = livingEntity.getKiller();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 8*20, 2));
        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 5*20, 1));
    }
}
