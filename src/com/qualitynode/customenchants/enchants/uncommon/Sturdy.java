package com.qualitynode.customenchants.enchants.uncommon;

import com.qualitynode.customenchants.handlers.types.ArmourEnchantment;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Sturdy extends ArmourEnchantment {

    @Override
    public String getName() {
        return "Sturdy";
    }

}
