package com.qualitynode.customenchants.enchants.uncommon;

import com.qualitynode.customenchants.events.enchants.BowEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.weapons.BowEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class HeadShot extends BowEnchantment {

    @Override
    public String getName() {
        return "HeadShot";
    }

    @EventHandler
    public void onBow(BowEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        event.setDamage(event.getOriginalDamage() * 2);
    }
}
