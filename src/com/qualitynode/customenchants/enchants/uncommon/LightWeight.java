package com.qualitynode.customenchants.enchants.uncommon;

import com.qualitynode.customenchants.events.enchants.WeaponEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.weapons.SwordEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class LightWeight extends SwordEnchantment {

    @Override
    public String getName() {
        return "LightWeight";
    }

    @EventHandler
    public void onWeapon(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 5*20, level-1));
    }
}
