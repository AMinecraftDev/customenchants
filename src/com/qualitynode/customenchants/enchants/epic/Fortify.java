package com.qualitynode.customenchants.enchants.epic;

import com.qualitynode.customenchants.events.enchants.ArmorEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.ArmourEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Fortify extends ArmourEnchantment {

    @Override
    public String getName() {
        return "Fortify";
    }

    @EventHandler
    public void onArmor(ArmorEnchantProcEvent event) {
        Player player = event.getPlayer();
        LivingEntity livingEntity = event.getTarget();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 5*20, level));
    }
}
