package com.qualitynode.customenchants.enchants.epic;

import com.qualitynode.customenchants.events.enchants.ToolEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.tools.PickaxeEnchantment;
import net.aminecraftdev.utils.RandomUtils;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class AutoSmelt extends PickaxeEnchantment {

    @Override
    public String getName() {
        return "AutoSmelt";
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onTool(ToolEnchantProcEvent event) {
        Player player = event.getPlayer();
        List<Block> blocks = event.getBlocks();
        int level = getEnchantLevel(player, this);
        int fortune = player.getItemInHand().hasItemMeta()? player.getItemInHand().getItemMeta().hasEnchant(Enchantment.LOOT_BONUS_BLOCKS) ? player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) : 1 : 1;

        if(!hasEnchant(player, this)) return;

        for(Block block : blocks) {
            if(!getOres().containsKey(block.getType())) continue;
            if(!canExecute(player, block.getLocation(), this, level)) continue;

            Material material = block.getType();
            int amount = (RandomUtils.getRandomInt(3) + 1) * fortune;
            short shortData = material == Material.LAPIS_ORE? (short) 4 : (short) 0;
            ItemStack itemStack = new ItemStack(getOres().get(block.getType()), amount > 64? 64 : amount, shortData);

            event.setDrops(block, itemStack);
        }
    }
}
