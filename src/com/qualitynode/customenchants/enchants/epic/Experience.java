package com.qualitynode.customenchants.enchants.epic;

import com.qualitynode.customenchants.events.enchants.ToolEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.tools.PickaxeEnchantment;
import net.aminecraftdev.utils.RandomUtils;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Experience extends PickaxeEnchantment {

    @Override
    public String getName() {
        return "Experience";
    }

    @EventHandler
    public void onTool(ToolEnchantProcEvent event) {
        Player player = event.getPlayer();
        List<Block> blocks = event.getBlocks();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;

        for(Block block : blocks) {
            if(!getOres().containsKey(block.getType())) continue;
            if(!canExecute(player, block.getLocation(), this, level)) continue;

            int expIncrease = (RandomUtils.getRandomInt(7) + 3) * level;
            event.setExpToDrop(event.getExpToDrop() + expIncrease);
        }
    }
}
