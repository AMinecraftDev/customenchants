package com.qualitynode.customenchants.enchants.epic;

import com.qualitynode.customenchants.events.enchants.WeaponEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.weapons.SwordEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Lifesteal extends SwordEnchantment {

    @Override
    public String getName() {
        return "Lifesteal";
    }

    @EventHandler
    public void onWeapon(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        double health = (player.getHealth() + level > player.getMaxHealth())? player.getMaxHealth() : (player.getHealth() + level);

        player.setHealth(health);
    }
}
