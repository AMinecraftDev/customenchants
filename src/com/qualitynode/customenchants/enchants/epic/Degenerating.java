package com.qualitynode.customenchants.enchants.epic;

import com.qualitynode.customenchants.events.enchants.WeaponEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.weapons.SwordEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Degenerating extends SwordEnchantment {

    @Override
    public String getName() {
        return "Degenerating";
    }

    @EventHandler
    public void onWeapon(WeaponEnchantProcEvent event) {
        Player player = event.getPlayer();
        LivingEntity livingEntity = event.getTarget();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 2 * 20, 2));
    }
}
