package com.qualitynode.customenchants.enchants.legendary;

import com.qualitynode.customenchants.events.enchants.ToolEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.ToolEnchantment;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 13-Aug-17
 */
public class Trenching extends ToolEnchantment {

    @Override
    public String getName() {
        return "Trenching";
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onTool(ToolEnchantProcEvent event) {
        Player player = event.getPlayer();
        List<Block> blocks = event.getBlocks();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        Block centerBlock = blocks.get(0);
        int blockX = centerBlock.getX();
        int blockY = centerBlock.getY();
        int blockZ = centerBlock.getZ();

        for(int x = blockX - level; x <= blockX + level; x++) {
            for(int y = blockY - level; y <= blockY + level; y++) {
                for(int z = blockZ - level; z <= blockZ + level; z++) {
                    Block block = centerBlock.getWorld().getBlockAt(x,y,z);

                    if(!canExecuteLocation(player, block.getLocation())) continue;
                    if(block.getType() == Material.BEDROCK) continue;
                    event.addBlock(block);
                }
            }
        }
    }



}
