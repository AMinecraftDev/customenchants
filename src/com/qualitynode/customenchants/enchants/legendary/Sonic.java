package com.qualitynode.customenchants.enchants.legendary;

import com.qualitynode.customenchants.handlers.types.armour.BootsEnchantment;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Sonic extends BootsEnchantment {

    @Override
    public String getName() {
        return "Sonic";
    }

}
