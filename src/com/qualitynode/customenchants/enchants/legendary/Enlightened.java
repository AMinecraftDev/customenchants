package com.qualitynode.customenchants.enchants.legendary;

import com.qualitynode.customenchants.events.enchants.ArmorEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.ArmourEnchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Enlightened extends ArmourEnchantment {

    @Override
    public String getName() {
        return "Enlightened";
    }

    @EventHandler
    public void onArmor(ArmorEnchantProcEvent event) {
        Player player = event.getPlayer();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        double health = (player.getHealth() + level > player.getMaxHealth())? player.getMaxHealth() : (player.getHealth() + level);

        player.setHealth(health);
    }
}
