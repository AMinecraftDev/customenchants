package com.qualitynode.customenchants.enchants.legendary;

import com.qualitynode.customenchants.events.enchants.ArmorEnchantProcEvent;
import com.qualitynode.customenchants.handlers.types.ArmourEnchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class Recoil extends ArmourEnchantment {

    @Override
    public String getName() {
        return "Recoil";
    }

    @EventHandler
    public void onArmor(ArmorEnchantProcEvent event) {
        Player player = event.getPlayer();
        LivingEntity target = event.getTarget();
        int level = getEnchantLevel(player, this);

        if(!hasEnchant(player, this)) return;
        if(!canExecute(player, this, level)) return;

        target.damage(level);
    }
}
