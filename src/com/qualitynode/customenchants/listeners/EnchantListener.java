package com.qualitynode.customenchants.listeners;

import com.qualitynode.customenchants.events.enchants.ArmorEnchantProcEvent;
import com.qualitynode.customenchants.events.enchants.BowEnchantProcEvent;
import com.qualitynode.customenchants.events.enchants.ToolEnchantProcEvent;
import com.qualitynode.customenchants.events.enchants.WeaponEnchantProcEvent;
import com.qualitynode.customenchants.handlers.EnchantHandler;
import com.qualitynode.customenchants.managers.EnchantManager;
import net.aminecraftdev.utils.RandomUtils;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 04-Aug-17
 */
public class EnchantListener implements Listener {

    private List<Material> tools = new ArrayList<>();

    public EnchantListener() {
        if(tools.isEmpty()) {
            tools.add(Material.WOOD_PICKAXE);
            tools.add(Material.STONE_PICKAXE);
            tools.add(Material.IRON_PICKAXE);
            tools.add(Material.GOLD_PICKAXE);
            tools.add(Material.DIAMOND_PICKAXE);

            tools.add(Material.WOOD_SPADE);
            tools.add(Material.STONE_SPADE);
            tools.add(Material.IRON_SPADE);
            tools.add(Material.GOLD_SPADE);
            tools.add(Material.DIAMOND_SPADE);

            tools.add(Material.WOOD_AXE);
            tools.add(Material.STONE_AXE);
            tools.add(Material.IRON_AXE);
            tools.add(Material.GOLD_AXE);
            tools.add(Material.DIAMOND_AXE);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onWeaponEnchant(EntityDamageByEntityEvent event) {
        if(event.isCancelled()) return;
        if(!(event.getDamager() instanceof Player)) return;
        if(!(event.getEntity() instanceof LivingEntity)) return;

        Player player = (Player) event.getDamager();
        LivingEntity livingEntity = (LivingEntity) event.getEntity();
        double damage = event.getDamage();

        WeaponEnchantProcEvent enchantProcEvent = new WeaponEnchantProcEvent(player, livingEntity, damage);

        Bukkit.getPluginManager().callEvent(enchantProcEvent);

        event.setCancelled(enchantProcEvent.isCancelled());
        event.setDamage(enchantProcEvent.getDamage());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onArmorEnchant(EntityDamageByEntityEvent event) {
        if(event.isCancelled()) return;
        if(!(event.getEntity() instanceof Player)) return;
        if(!(event.getDamager() instanceof LivingEntity)) return;

        Player player = (Player) event.getEntity();
        LivingEntity livingEntity = (LivingEntity) event.getDamager();
        double damage = event.getDamage();

        ArmorEnchantProcEvent armorEnchantProcEvent = new ArmorEnchantProcEvent(player, livingEntity, damage);

        Bukkit.getPluginManager().callEvent(armorEnchantProcEvent);

        event.setCancelled(armorEnchantProcEvent.isCancelled());
        event.setDamage(armorEnchantProcEvent.getDamage());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onToolEnchant(BlockBreakEvent event) {
        if(event.isCancelled()) return;

        Player player = event.getPlayer();
        List<Block> blocks = new ArrayList<>();
        int expToDrop = event.getExpToDrop();
        ItemStack itemStack = player.getItemInHand();

        if(player.getGameMode() == GameMode.CREATIVE) return;
        if(!isTool(itemStack)) return;

        blocks.add(event.getBlock());

        ToolEnchantProcEvent toolEnchantProcEvent = new ToolEnchantProcEvent(player, blocks, expToDrop);

        Bukkit.getPluginManager().callEvent(toolEnchantProcEvent);

        event.setCancelled(toolEnchantProcEvent.isCancelled());
        event.setExpToDrop(toolEnchantProcEvent.getExpToDrop());

        if(event.isCancelled()) return;

        event.setCancelled(true);

//        boolean didItemBreak = false;

        if(EnchantManager.hasEnchant(player.getItemInHand(), EnchantHandler.AutoSmelt.getEnchantment()) || EnchantManager.hasEnchant(player.getItemInHand(), EnchantHandler.Trenching.getEnchantment())) {
            for(Block block : toolEnchantProcEvent.getBlocks()) {
//                if(didItemBreak) {
//                    return;
//                }

                if(toolEnchantProcEvent.getDrops().containsKey(block)) {
                    block.getWorld().dropItemNaturally(block.getLocation(), toolEnchantProcEvent.getDrops().get(block));
                    block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, block.getType());
                    block.setType(Material.AIR);
                } else {
                    block.breakNaturally();
                }

//                if(canHurtItem(itemStack)) {
//                    if(itemStack.getDurability() == itemStack.getType().getMaxDurability()) {
//                        didItemBreak = true;
//                        player.setItemInHand(new ItemStack(Material.AIR));
//                    } else {
//                        itemStack.setDurability((short) (itemStack.getDurability() + 1));
//                    }
//                }
            }
            if(canHurtItem(itemStack)) {
                if(itemStack.getDurability() == itemStack.getType().getMaxDurability()) {
                    player.setItemInHand(new ItemStack(Material.AIR));
                } else {
                    itemStack.setDurability((short) (itemStack.getDurability() + 1));
                }
            }
        } else {
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBowEnchant(EntityDamageByEntityEvent event) {
        if(event.isCancelled()) return;
        if(event.getCause() != EntityDamageEvent.DamageCause.PROJECTILE) return;
        if(!(event.getEntity() instanceof LivingEntity)) return;

        Projectile projectile = (Projectile) event.getDamager();
        LivingEntity shooter = (LivingEntity) projectile.getShooter();

        if(!(shooter instanceof Player)) return;

        Player player = (Player) shooter;
        LivingEntity target = (LivingEntity) event.getEntity();
        double damage = event.getDamage();

        BowEnchantProcEvent bowEnchantProcEvent = new BowEnchantProcEvent(player, target, damage);

        Bukkit.getPluginManager().callEvent(bowEnchantProcEvent);

        event.setCancelled(bowEnchantProcEvent.isCancelled());
        event.setDamage(bowEnchantProcEvent.getDamage());
    }

    private boolean isTool(ItemStack itemStack) {
        return tools.contains(itemStack.getType());
    }

    private boolean canHurtItem(ItemStack itemStack) {
        if(!itemStack.getItemMeta().hasEnchant(Enchantment.DURABILITY)) return true;

        int level = itemStack.getEnchantmentLevel(Enchantment.DURABILITY);
        int randomNumber = RandomUtils.getRandomInt(100) + 1;

        return randomNumber < (100 - (10 * level));
    }

}
