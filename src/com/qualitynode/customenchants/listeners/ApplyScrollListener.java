package com.qualitynode.customenchants.listeners;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.managers.*;
import com.qualitynode.customenchants.utils.Rarity;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Aug-17
 */
public class ApplyScrollListener implements Listener {

    private final Material blackScrollType, dustType, whiteScrollType;

    public ApplyScrollListener(CustomEnchants plugin) {
        this.blackScrollType = ItemStackUtils.getType(plugin.getConfig().getString("CustomEnchants.Items.BlackScroll.type"));
        this.dustType = ItemStackUtils.getType(plugin.getConfig().getString("CustomEnchants.Items.Dust.type"));
        this.whiteScrollType = ItemStackUtils.getType(plugin.getConfig().getString("CustomEnchants.Items.WhiteScroll.type"));
    }

    @EventHandler
    public void onBlackScrollApply(InventoryClickEvent event) {
        if(!(event.getWhoClicked() instanceof Player)) return;
        if(event.getCursor() == null || event.getCursor().getType() != this.blackScrollType) return;
        if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR) return;

        Player player = (Player) event.getWhoClicked();
        ItemStack cursor = event.getCursor();
        ItemStack currentItem = event.getCurrentItem();

        for(ItemStack itemStack : player.getEquipment().getArmorContents()) {
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;

            if(itemStack.isSimilar(currentItem)) {
                event.setCancelled(true);
                return;
            }
        }

        if(!BlackScrollManager.isBlackScroll(cursor)) return;
        if(!EnchantManager.hasEnchants(currentItem)) return;
        if(!event.getClickedInventory().equals(player.getInventory())) return;

        event.setCancelled(true);

        if(cursor.getAmount() > 1) {
            ItemStack clone = cursor.clone();

            clone.setAmount(cursor.getAmount() - 1);
            event.setCursor(clone);
        } else {
            event.setCursor(new ItemStack(Material.AIR));
        }

        event.setCurrentItem(BlackScrollManager.removeRandomEnchant(player, currentItem));
        Message.CustomEnchants_BlackScroll_Apply.msg(player);
    }

    @EventHandler
    public void onDustApply(InventoryClickEvent event) {
        if(!(event.getWhoClicked() instanceof Player)) return;
        if(event.getCursor() == null || event.getCursor().getType() != this.dustType) return;
        if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR) return;

        Player player = (Player) event.getWhoClicked();
        ItemStack cursor = event.getCursor();
        ItemStack currentItem = event.getCurrentItem();

        for(ItemStack itemStack : player.getEquipment().getArmorContents()) {
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;

            if(itemStack.isSimilar(currentItem)) {
                Message.CustomEnchants_Dust_CannotApplyArmor.msg(player);
                event.setCancelled(true);
                return;
            }
        }

        if(!DustManager.isDust(cursor)) return;
        if(!EnchanterManager.hasEnchantmentTag(currentItem)) return;
        if(!EnchanterManager.hasLevelTag(currentItem)) return;
        if(!EnchanterManager.hasSuccessRate(currentItem)) return;
        if(!EnchanterManager.hasDestroyRate(currentItem)) return;

        CEnchantment currentItemEnchantment = EnchanterManager.getEnchantment(currentItem);

        if(currentItemEnchantment == null) return;

        event.setCancelled(true);

        Rarity ciRarity = currentItemEnchantment.getRarity();
        Rarity cuRarity = DustManager.getDustRarity(cursor);
        int increaseAmount = DustManager.getDustAmount(cursor);
        int currentSuccess = EnchanterManager.getSucessRate(currentItem);

        if(ciRarity != cuRarity) {
            Message.CustomEnchants_Dust_CannotMixRarities.msg(player);
            return;
        }

        if(currentSuccess == 100) {
            Message.CustomEnchants_Dust_AlreadyMaxed.msg(player);
            return;
        }

        if((increaseAmount + currentSuccess) > 100) {
            increaseAmount = 100 - currentSuccess;
        }

        if(cursor.getAmount() > 1) {
            ItemStack clone = cursor.clone();

            clone.setAmount(cursor.getAmount() - 1);
            event.setCursor(clone);
        } else {
            event.setCursor(new ItemStack(Material.AIR));
        }

        event.setCurrentItem(EnchanterManager.increaseSuccessRate(currentItem, increaseAmount));
        Message.CustomEnchants_Dust_Successful.msg(player);
    }

    @EventHandler
    public void onWhiteScrollApply(InventoryClickEvent event) {
        if(!(event.getWhoClicked() instanceof Player)) return;
        if(event.getCursor() == null || event.getCursor().getType() != this.whiteScrollType) return;
        if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR) return;

        Player player = (Player) event.getWhoClicked();
        ItemStack cursor = event.getCursor();
        ItemStack currentItem = event.getCurrentItem();

        for(ItemStack itemStack : player.getEquipment().getArmorContents()) {
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;

            if(itemStack.isSimilar(currentItem)) {
                event.setCancelled(true);
                return;
            }
        }

        if(!WhiteScrollManager.isWhiteScroll(cursor)) return;
        if(!EnchantManager.canApplyScroll(currentItem)) return;
        if(!event.getClickedInventory().equals(player.getInventory())) return;

        event.setCancelled(true);

        if(WhiteScrollManager.hasWhitescroll(currentItem)) {
            Message.CustomEnchants_WhiteScroll_AlreadyHas.msg(player);
            return;
        }

        if(cursor.getAmount() > 1) {
            ItemStack clone = cursor.clone();

            clone.setAmount(cursor.getAmount() - 1);
            event.setCursor(clone);
        } else {
            event.setCursor(new ItemStack(Material.AIR));
        }

        event.setCurrentItem(WhiteScrollManager.addWhiteScrollEffect(currentItem));
        Message.CustomEnchants_WhiteScroll_Successful.msg(player);
    }

}
