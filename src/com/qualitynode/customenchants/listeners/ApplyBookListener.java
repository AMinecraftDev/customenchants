package com.qualitynode.customenchants.listeners;

import com.qualitynode.customenchants.CustomEnchants;
import com.qualitynode.customenchants.handlers.CEnchantment;
import com.qualitynode.customenchants.managers.EnchantManager;
import com.qualitynode.customenchants.managers.EnchanterManager;
import com.qualitynode.customenchants.managers.WhiteScrollManager;
import com.qualitynode.customenchants.utils.core.Message;
import net.aminecraftdev.utils.itemstack.ItemStackUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 07-Aug-17
 */
public class ApplyBookListener implements Listener {

    private final Material bookType;

    public ApplyBookListener(CustomEnchants plugin) {
        this.bookType = ItemStackUtils.getType(plugin.getConfig().getString("CustomEnchants.Items.OpenedEnchantBook.type"));
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(!(event.getWhoClicked() instanceof Player)) return;
        if(event.getCursor() == null) return;
        if(event.getCursor().getType() != this.bookType) return;
        if(event.getCurrentItem() == null) return;

        Player player = (Player) event.getWhoClicked();
        ItemStack cursor = event.getCursor();
        ItemStack itemInSlot = event.getCurrentItem();

        for(ItemStack itemStack : player.getEquipment().getArmorContents()) {
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;

            if(itemStack.isSimilar(itemInSlot)) {
                event.setCancelled(true);
                return;
            }
        }

        if(!EnchanterManager.hasEnchantmentTag(cursor)) return;
        if(!EnchanterManager.hasLevelTag(cursor)) return;
        if(!EnchanterManager.hasSuccessRate(cursor)) return;
        if(!EnchanterManager.hasDestroyRate(cursor)) return;

        CEnchantment enchantment = EnchanterManager.getEnchantment(cursor);
        int destroyRate = EnchanterManager.getDestroyRate(cursor);
        int successRate = EnchanterManager.getSucessRate(cursor);
        int enchantLevel = EnchanterManager.getLevel(cursor);

        if(!EnchantManager.canEnchantItem(itemInSlot, enchantment)) return;

        event.setCancelled(true);

        int currentAmount = EnchantManager.getEnchantsAmount(itemInSlot);
        int currentLevel = EnchantManager.getEnchantLevel(itemInSlot, enchantment);
        boolean alreadyHas = EnchantManager.hasEnchant(itemInSlot, enchantment);

        if((currentAmount >= EnchantManager.getMaxEnchantsAllowed(player)) && (!alreadyHas)) {
            Message.CustomEnchants_MaxEnchants.msg(player, EnchantManager.getMaxEnchantsAllowed(player));
            return;
        }

        if((alreadyHas) && (currentLevel >= enchantLevel)) {
            Message.CustomEnchants_AlreadyHaveEnchant.msg(player);
            return;
        }

        if(cursor.getAmount() > 1) {
            cursor.setAmount(cursor.getAmount() - 1);
        } else {
            event.setCursor(new ItemStack(Material.AIR));
        }


        if(!EnchanterManager.isApplicationSuccessful(successRate)) {
            if(EnchanterManager.isApplicationSuccessful(destroyRate)) {
                if(WhiteScrollManager.hasWhitescroll(itemInSlot)) {
                    Message.CustomEnchants_Apply_WhiteScroll.msg(player);
                    event.setCurrentItem(WhiteScrollManager.takeWhiteScrollEffect(itemInSlot));
                } else {
                    Message.CustomEnchants_Apply_NoWhiteScroll.msg(player);
                    event.setCurrentItem(new ItemStack(Material.AIR));
                }

                return;
            }

            Message.CustomEnchants_Apply_NoDestroy.msg(player);
        } else {
            event.setCurrentItem(EnchantManager.enchantItem(itemInSlot, enchantment, enchantLevel, currentAmount));
            Message.CustomEnchants_Apply_Successful.msg(player);
        }
    }

}
